def channel_command(self, channel, message):
    game = self.get_game()

    if not message:
        self.echo("%s what?" % (channel["name"]))
        return

    send_check = channel.get("send_check")
    if send_check and not send_check(self):
        self.echo(channel.get("send_error", "You can't use that channel."))
        return

    # TODO act template replacer
    to_self = channel["to_self"]
    to_self = to_self.replace("{actor.name}", self.name)
    to_self = to_self.replace("{message}", message)

    self.echo(to_self)

    receive_check = channel.get("receive_check")

    for player in game.get_players():
        if player.name == self.name:
            continue

        if receive_check and not receive_check(self, player):
            continue

        # TODO act template replacer
        # TODO add visibility checks
        to_other = channel["to_other"]
        to_other = to_other.replace("{actor.name}", self.name)
        to_other = to_other.replace("{message}", message)
        player.echo(to_other)
