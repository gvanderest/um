"""MUD Connections."""
from utils.hash import get_random_hash


class Connection(object):
    """
    MUD Connection.

    How a socket or player sends commands for their Character to execute.
    """

    def __init__(self, game, socket, address):
        """Wrap a socket of some kind."""
        self.game = game
        self.set_socket(socket)
        self.set_address(address)

    def get_and_clear_buffer(self):
        """Get the contents and clear the buffer."""
        outgoing = self.buffer
        self.buffer = ""
        return outgoing

    def set_socket(self, socket):
        """Set the raw socket being used."""
        self.socket = socket

    def get_socket(self):
        """Return the raw socket being used."""
        return self.socket

    def set_address(self, address):
        """Set the address/port tuple."""
        self.address = address

        # Also lookup the hostname as needed.
        self.hostname = 'unknown'

    def get_address(self):
        """Return a tuple of the address/port being used."""
        return self.address

    def handle_input(self, message):
        """Parse the raw information coming from the socket."""
        pass

    def write(self, message):
        """Write output to the socket."""
        self.socket.write(message)

    def write_line(self, message):
        """Write a line of output to the socket."""
        self.write(message + "\n")
