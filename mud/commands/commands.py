def commands_command(self, arguments):
    """List the commands available."""
    from settings import COMMANDS
    keywords = []
    for entry in COMMANDS:
        keywords.append(entry["keyword"])
        for alias in entry.get("aliases", []):
            keywords.append(alias)

    for keyword in sorted(keywords):
        self.echo(keyword)
