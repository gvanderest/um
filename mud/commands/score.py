def score_command(self, arguments):
    title = self.title or ""
    self.echo("{GName{g:{x %s %s{x" % (self.name, title))
    self.echo("""\
{GDesc{g:{x True Neutral Male Human Adventurer {GLevel{g:{x 1 {GAge{g:{x 1
{GHP{g:{x1234/1234 {g({c1234{g) {GMana{g:{x1234/1234 {g({c1234{g) {GMove{g:{x1234/1234 {g({c998{g)
{g------------+-----------------------+---------------
{GStr{g:{x  1{g({x 1{g) {g|{G Items{g:{x     96{g/{x186     {g|{G Hitroll{g:{x 254
{GInt{g:{x  1{g({x 1{g) {g|{G Weight{g:{x   276{g/{x752     {g|{G Damroll{g:{x 285
{GWis{g:{x  1{g({x 1{g) +----------------+------+---------------
{GDex{g:{x  1{g({x 1{g) {g|{G Practices{g:{x  28 {g|{G Arena Wins{g:{x     0
{GCon{g:{x  1{g({x 1{g) {g|{G Trains{g:{x      0 {g|{G Arena Losses{g:{x   0
{g------------+---------+------+----------------------
{GCoins Platinum{g:{x     0 {g|{G Experience
      {GGold{g:{x         0 {g|{G  Current{g:{x 0
      {GSilver{g:{x       0 {g|{G Adventure Points{g:{x 0
{g----------------------+-----------------------------
{GArmor Pierce{g:{x  -390 [{Rarm{rored {8like a bri{rck ho{Ruse{x]
      {GBash{g:{x    -384 [{Rarm{rored {8like a bri{rck ho{Ruse{x]
      {GSlash{g:{x   -390 [{Rarm{rored {8like a bri{rck ho{Ruse{x]
      {GMagic{g:{x   -369 [{Rarm{rored {8like a bri{rck ho{Ruse{x]
{g----------------+-------------+---------------------
{GAlignment{g:{x    0 {g|{G Wimpy{g:{x    0 {g|{G Quest Points{g:{x     0
{g----------------+-------------+---------------------
{GPkStatus{g:{x NPK
{g----------------------------------------------------{x""")
