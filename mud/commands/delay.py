def delay_command(self, arguments):
    """Set the command delay of this Connection."""

    if not arguments:
        self.echo("Please provide the connection delay in seconds")
        self.echo("Min: 0.01")
        self.echo("Max: 5.00")
        return

    value = float(arguments[0])

    value = max(0.00, value)
    value = min(5.00, value)

    connection = self.get_connection()
    connection.set_command_delay(value)
    self.echo("Command delay set to %s seconds." % (value))
