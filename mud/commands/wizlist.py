def wizlist_command(self, arguments):
    self.echo("""\
  ___________________________________________________________________________
 /\\_\\                                                                      \\_\\
|/\\\\_\\                    {WGods and Rulers Of Waterdeep{x                      \\_\\
\\_/_|_|""" + (" " * 70) +"""|_|""")
    immortal_names = [
        "Kelemvor",
        "Mielikki",
        "Bahamut",
        "Vorcet",
        "Torog",
        "Sharess",
        "Jergal"
    ]
    past_immortal_names = [
        "Nisstyre",
        "Kord",
        "Bane",
        "Malar",
        "Sune",
    ]

    self.echo("    |_|%s|_|" % (" " * 70))

    self.echo("    |_|{R%s{x|_|" % ("Immortals".center(70)))
    self.echo("    |_|{Y%s{x|_|" % ("*********".center(70)))

    for name in immortal_names:
        self.echo("    |_|{G%s{x|_|" % (name.center(70)))

    self.echo("    |_|%s|_|" % (" " * 70))
    self.echo("    |_|%s|_|" % (" " * 70))

    self.echo("    |_|{R%s{x|_|" % ("Past Immortals".center(70)))
    self.echo("    |_|{Y%s{x|_|" % ("**************".center(70)))

    for name in past_immortal_names:
        self.echo("    |_|{G%s{x|_|" % (name.center(70)))

    self.echo("""\
 ___|_|                                                                      |_|
/ \\ |_|""" + (70 * " ") + """|_|
|\\//_/                                                                      /_/
 \\/_/______________________________________________________________________/_/""")
