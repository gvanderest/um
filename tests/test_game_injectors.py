"""Test Game Injectors and Injection."""
from mud.game import Game


class ClassInjector(object):
    """Class-style Injector."""

    def __init__(self, game):
        """Bind the Game."""
        self.game = game


def some_injector(game):
    """Function-based Injector."""
    return game


class TestGameInjectors(object):
    """Test Injectors work properly with the Game."""

    def setUp(self):
        """Instantiate Game."""
        self.game = Game()

    def test_add_remove_function_injector(self):
        """Add and remove an Injector from the Game."""
        assert len(self.game.get_injectors()) == 0

        self.game.add_injector("test", some_injector)

        assert len(self.game.get_injectors()) == 1

        def function_to_inject(test):
            assert test is self.game

        self.game.inject(function_to_inject)

        self.game.remove_injector("test")

        assert len(self.game.get_injectors()) == 0

    def test_clearing(self):
        """After adding, test that clearing works."""
        self.game.add_injector("test", some_injector)
        self.game.add_injector("test2", some_injector)
        self.game.add_injector("test3", some_injector)

        assert len(self.game.get_injectors()) == 3

        self.game.clear_injectors()

        assert len(self.game.get_injectors()) == 0

    def test_invalid_injector(self):
        """Test requesting and injecting invalid injectors."""
        def invalid_func(xxx):
            pass

        failed = False
        try:
            self.game.inject(invalid_func)
        except KeyError:
            failed = True

        assert failed

    def test_add_remove_class_injector(self):
        """Add and remove an Injector from the Game."""
        assert len(self.game.get_injectors()) == 0

        self.game.add_injector("test2", ClassInjector)

        def function_to_inject(test2):
            assert isinstance(test2, ClassInjector)
            assert test2.game is self.game

        assert len(self.game.get_injectors()) == 1

        self.game.inject(function_to_inject)

        self.game.remove_injector("test2")

        assert len(self.game.get_injectors()) == 0
