def look_command(self, arguments=None):
    from settings import DIRECTIONS

    room = self.get_room()

    title_line = "{B" + room.name + "{x"

    if room.has_flag("law"):
        title_line += " {w[{WLAW{w]{x"

    if room.has_flag("safe"):
        title_line += " {R[{WSAFE{R]{x"

    if room.has_flag("city"):
        title_line += " {x(%s{x)" % ("{Wcityst")

    title_line += " {x[Room %s]{x" % (room.vnum)

    self.echo(title_line)

    from mud.commands.map import Map
    MAP_WIDTH = 11
    MAP_HEIGHT = 7
    MAP_BORDER = True
    minimap = Map.generate_map(room, width=MAP_WIDTH, height=MAP_HEIGHT,
                               border=MAP_BORDER)
    map_lines = minimap.get_lines()

    num_description_lines = len(room.description)

    for index in range(max(num_description_lines, MAP_HEIGHT)):
        line = ""
        if index >= MAP_HEIGHT:
            line += (" " * MAP_WIDTH) + "  "
        else:
            line += map_lines[index] + "  "

        if index < num_description_lines:
            if index == 0:
                line += "{x  "
            line += room.description[index]

        self.echo(line)

    self.echo()

    exits = []
    doors = []
    secrets = []

    for direction in DIRECTIONS:
        exit = room.get_exit(direction["id"])

        if not exit:
            continue

        if exit.has_flag("door") and exit.has_flag("closed"):
            if exit.has_flag("secret"):
                secrets.append(direction)
            else:
                doors.append(direction)
        else:
            exits.append(direction)

    def format_exits(exits):
        if not exits:
            return "{xnone"
        return " ".join(map(lambda exit: exit["name"], exits))

    exit_line = ""
    exit_line += "{g[{GExits{g: " + format_exits(exits) + "{g]   "
    exit_line += "{g[{GDoors{g: " + format_exits(doors) + "{g]   "
    exit_line += "{g[{GSecrets{g: " + format_exits(secrets) + "{g]   "
    exit_line += "{x"

    self.echo(exit_line)

    for actor in room.get_actors(exclude=self):
        self.echo("%s{x is here." % (actor.name))
