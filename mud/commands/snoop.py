def snoop_command(self, arguments):
    """Watch the output of another Actor."""
    if not arguments:
        self.echo("You must provide a name of someone you wish to snoop.")
        return

    name_find = arguments[0]
    # FIXME use collection to find them
    target = find_character(self.game, name_find)
    if not target:
        self.echo("Character '%s' not found." % (name_find))
        return

    # FIXME use IDs
    if target.name == self.name:
        self.echo("You can not snoop yourself.")
        return

    if target.is_snooping(self):
        self.echo("You can't snoop someone who is snooping you.")
        return

    if not self.is_snooping(target):
        self.echo("You are now snooping %s." % (target.name))
        self.snoop(target)
    else:
        self.echo("You are no longer snooping %s." % (target.name))
        self.unsnoop(target)
