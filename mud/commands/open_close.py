def open_or_close_command(self, action, arguments):
    """Toggle an exit open or closed, to reduce code duplication."""
    from settings import DIRECTIONS

    if not arguments:
        if action == "open":
            self.echo("Open what?")
        else:
            self.echo("Close what?")
        return

    room = self.get_room()

    # FIXME use a common function, injector for Directions
    exit = None
    for direction in DIRECTIONS:
        if direction["id"].startswith(arguments[0]):
            exit = room.get_exit(direction["id"])
            break

    if not exit or not exit.has_flag("door"):
        self.echo("There is no door in that direction")
        return

    if action == "open" and not exit.has_flag("closed"):
        self.echo("That door is already open.")
        return
    elif action == "close" and exit.has_flag("closed"):
        self.echo("That door is already closed.")
        return

    other_room = exit.get_room()
    other_room_exit = other_room.get_exit(direction["opposite_id"])

    if action == "open":
        exit.remove_flag("closed")
        other_room_exit.remove_flag("closed")
        self.echo("You open the door.")
        self.act_to_room("{actor.name} opens the door.")
        self.act_to_room("The door opens.", room=other_room)
    else:
        exit.add_flag("closed")
        other_room_exit.add_flag("closed")
        self.echo("You close the door.")
        self.act_to_room("{actor.name} closes the door.")
        self.act_to_room("The door closes.", room=other_room)

def close_command(self, arguments):
    open_or_close_command(self, "close", arguments)

def open_command(self, arguments):
    open_or_close_command(self, "open", arguments)
