def afk_command(self, arguments):
    """Toggle between AFK and active."""
    if self.has_flag(self.AFK_FLAG):
        self.remove_flag(self.AFK_FLAG)
        self.echo("AFK mode removed.  You have no tells waiting.")
    else:
        self.add_flag(self.AFK_FLAG)
        self.echo("You are now in AFK mode.")
