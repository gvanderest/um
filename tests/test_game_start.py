"""Test the Game being started for a few ticks."""
from mud.manager import Manager
from mud.game import Game


class MockManager(Manager):
    """Capture some Events."""

    def start(self):
        """Create container for Events."""
        self.events = []

    def handle_event(self, event):
        """Capture Events."""
        self.events.append(event)


class TestGameStart(object):
    """Test the basics of the Game."""

    def setUp(self):
        """Instantiate the Game with the MockModule/MockManager."""
        self.game = Game()
        self.manager = MockManager(self.game)
        self.game.add_manager(self.manager)

    def test_a_few_ticks(self):
        """We should collect a few ticks of time if the Game runs."""
        self.game.start(iterations=10, tick_delay=0)

        ticks = [
            event for event in self.manager.events
            if event.type == "GAME_TICK"
        ]

        assert len(ticks) == 10
