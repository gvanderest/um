"""Undermountain Game."""

import gevent
import importlib
import inspect
import json
import logging
import sys
import time
import traceback

from mud.event import Event


class Game(object):
    """Container of everything that exists in the game."""

    VERSION = None

    def __init__(self, environment=None, state=None, modules=None,
                 log=False, data_path='data'):
        """Create the Game and its expected services."""
        self.environment = environment

        self.set_state(state)
        self.modules = []
        self.managers = []
        self.injectors = {}
        self.log_events = log  # FIXME name this flag better
        self.data_path = data_path  # FIXME suse set_data_path
        self.connections = {}
        self.logging = True

        self.add_modules(modules)

    @classmethod
    def get_version(cls):
        """Read the version."""
        if cls.VERSION is None:
            try:
                with open("VERSION", "r") as fh:
                    cls.VERSION = fh.read().strip()
            except Exception:
                cls.VERSION = "UNKNOWN"

        return cls.VERSION

    # TODO move this to Characters collection
    def find_player(self, name, exclude=None):
        """Find a specific player."""
        name = name.lower()
        for player in self.get_players():
            if player == exclude:
                continue

            if player.name.lower().startswith(name):
                return player

    def get_players(self):
        """Get the Actors of the Players of the game."""
        for connection in self.get_connections():
            if connection.actor is None:
                continue
            yield connection.actor

    def get_connection(self, id):
        """Return a specific connection by its ID."""
        return self.connections.get(id, None)

    def get_connections(self):
        """Generate all Connections to the Game."""
        for connection in self.connections.items():
            yield connection[1]

    def add_connection(self, connection):
        """Add a Connection to the Game."""
        self.connections[connection.id] = connection

    def remove_connection(self, connection):
        """Remove a Connection from the Game."""
        del self.connections[connection.id]

    def add_modules(self, modules):
        """Add a list of Modules."""
        if modules is None:
            return

        for module in modules:
            self.add_module(module)

    def set_state(self, state):
        """Set the State of the Game."""
        self.state = state or {}

    def clear_state(self):
        """Clear the State of the Game."""
        self.set_state({})

    def get_state(self):
        """Get a copy of the State of the Work."""
        return dict(self.state)

    def create_snapshot(self):
        """Get a snapshot (pickled) of the Game State."""
        return json.dumps(self.state)

    def load_snapshot(self, snapshot):
        """Revert to a snapshot (pickled) of a Game State."""
        state = json.loads(snapshot)
        self.set_state(state)

    def clear_injectors(self):
        """Remove all injectors."""
        for name in dict(self.injectors):
            self.remove_injector(name)

    def add_injector(self, name, injector):
        """Register Injector to Game."""
        self.injectors[name] = injector(self)

    def get_injectors(self):
        """Get the list of Injectors."""
        return self.injectors

    def remove_injector(self, name):
        """Remove Injector from Game."""
        del self.injectors[name]

    def get_injector(self, name):
        """Return Injector by its name."""
        if name not in self.injectors:
            raise KeyError("Injector with name '{}' not found.".format(name))

        return self.injectors[name]

    def get_function_injections(self, function, overrides=None):
        """Get the Injections that will be passed to the function."""
        import inspect

        injectors = dict(self.injectors)

        # Append any overrides.
        injectors.update(overrides or {})

        arg_names_to_inject = inspect.getargspec(function)[0]
        injections = {}

        function_is_method = inspect.ismethod(function)

        for arg_name in arg_names_to_inject:
            if function_is_method and arg_name == "self":
                continue
            injections[arg_name] = injectors[arg_name]

        return injections

    def inject(self, function, overrides=None):
        """Inject a function with Injectors, plus any overrides."""
        injectors = self.get_function_injections(function, overrides)
        return function(**injectors)

    def log(self, message, data=None):
        """Log anything."""
        if data is None:
            data = {}

        logging.info(message, extra=data)

    def log_event(self, event):
        """Take an Event and log it."""
        self.log("{}: {}".format(
            event.type,
            repr(event.data),
        ))

    def dispatch(self, event, *args, **kwargs):
        """Handle an Event that occurs to all Modules."""
        if not isinstance(event, Event):
            event = Event(event, *args, **kwargs)

        self.log_event(event)

        for manager in self.managers:
            if event.type in manager.HANDLES_EVENTS:
                manager.handle_event(event)

    def handle_exception(self):
        """Handle an Exception that just occurred."""
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
        output = "\n".join(lines)
        self.dispatch("EXCEPTION", {"traceback": output})

    def get_modules(self):
        """Return list of Modules."""
        return self.modules

    def remove_module(self, provided):
        """Remove modules that already exist by provided class."""
        self.modules = [
            module
            for module in self.modules
            if module is not provided
        ]

    def add_module(self, module):
        """Register a Module to the Game."""
        # Unwrap a string-based module name.
        if isinstance(module, str):
            module_parts = module.split(".")
            module_name = module_parts.pop(-1)
            package = importlib.import_module(".".join(module_parts))
            module = getattr(package, module_name)

        self.remove_module(module)
        self.modules.append(module)

        for manager_class in module.MANAGERS:
            self.add_manager(manager_class)

        for injector_id, injector_class in module.INJECTORS.items():
            self.add_injector(injector_id, injector_class)

    def add_manager(self, manager_class):
        """Register a Manager to the Game."""
        manager = manager_class(self)
        self.managers.append(manager)
        manager.start()

    def clear_modules(self):
        """Remove all Modules from Game."""
        self.modules = []

    def start_thread(self, tick_delay=None, iterations=None):
        """Primary Game thread."""
        tick_delay = 1.0 if tick_delay is None else tick_delay

        self.running = True
        self.dispatch(Event("GAME_STARTING"))
        self.dispatch(Event("GAME_STARTED"))

        count = 0
        while self.running:
            count += 1

            self.dispatch(Event("GAME_TICK", {
                "timestamp": time.time(),
                "count": count,
                "connections": len(self.connections),
            }))

            if count == iterations:
                break

            gevent.sleep(tick_delay)

        self.dispatch(Event("GAME_STOPPING"))
        self.dispatch(Event("GAME_STOPPED"))

    def start(self, **kwargs):
        """Start the primary Game thread."""
        gevent.spawn(self.start_thread, **kwargs).join()
