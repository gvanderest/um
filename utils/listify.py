def listify(value):
    if isinstance(value, list):
        return list(value)

    elif value is not None:
        return [value]

    return []
