"""Test Game Modules and Events."""
from mud.event import Event
from mud.game import Game
from mud.module import Module
from mud.manager import Manager
from mud.exceptions import InvalidEvent


class MockManager(Manager):
    """Manager example for holding on to test Events."""

    def start(self):
        """Make a little Event holder."""
        self.events = []

    def handle_event(self, event):
        """Capture Event and data."""
        self.events.append(event)


class MockModule(Module):
    """Module example for holding on to test Events."""

    MANAGERS = [MockManager]


class TestGameModules(object):
    """Test Modules from Game work properly."""

    def setUp(self):
        """Instantiate Game."""
        self.game = Game()

    def test_add_remove(self):
        """Test registration and removal of Modules."""
        # Start with none.
        modules = self.game.get_modules()
        assert len(modules) == 0

        # Add one.
        mock = MockModule
        self.game.add_module(mock)

        modules = self.game.get_modules()
        assert len(modules) == 1

        # Duplicate not allowed.
        self.game.add_module(mock)

        class SecondFakeModule(Module):
            def __init__(self, game):
                pass

        # Add a secondary dummy module.
        fake = SecondFakeModule(self.game)
        self.game.add_module(fake)

        modules = self.game.get_modules()
        assert len(modules) == 2

        # Unadd one.
        self.game.remove_module(fake)

        modules = self.game.get_modules()
        assert len(modules) == 1

        # Clear the remainder.
        self.game.clear_modules()

        modules = self.game.get_modules()
        assert len(modules) == 0

    def test_dispatch(self):
        """Test handling of Events by Modules."""
        manager = MockManager(self.game)
        self.game.add_manager(manager)
        self.game.dispatch(Event("example_event", {"abc": 123}))

        assert len(manager.events) == 1

        event = manager.events[0]
        assert isinstance(event, Event)
        assert event.type == "example_event"
        assert event.data["abc"] == 123

    def test_invalid_dispatch_object(self):
        """Test handling of a dispatch for a non-Event."""
        failed = False

        try:
            self.game.dispatch("banana")
        except InvalidEvent:
            failed = True

        assert failed

    def test_instantiation_with_game(self):
        """Test that Modules can be provided on instantiation."""
        game = Game(modules=[MockModule])
        assert len(game.get_modules()) == 1
