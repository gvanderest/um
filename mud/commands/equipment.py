def equipment_command(self, arguments):
    """List an Actor's equipment."""
    from utils.ansi import Ansi
    from settings import EQUIPMENT_SLOTS

    for slot in EQUIPMENT_SLOTS:
        self.echo("%s {x%s" % (
            Ansi.pad_right("{G<{C%s{G>" % (slot.get("label", slot["id"])), 14),
            "Nothing",
        ))
