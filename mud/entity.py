class Entity(object):
    def __init__(self, data):
        super(Entity, self).__setattr__('data', data)

    def __getattr__(self, key):
        return self.data.get(key, None)

    def __delattr__(self, key):
        if key in self.data:
            del self.data[key]

    def __setattr__(self, key, val):
        self.data[key] = val

    def __getitem__(self, key):
        return self.__getattr__(key)

    def __setitem__(self, key, val):
        return self.__setattr__(key, val)

    def __delitem__(self, key):
        return self.__delattr__(key)

    def __eq__(self, other):
        try:
            if self.id is None or other.id is None:
                return False
            return self.id == other.id
        except AttributeError:
            return False

    def __neq__(self, other):
        return not self.__eq__(other)

    def get(self, key, default=None):
        return self.data.get(key, default)
