def quit_command(self, arguments):
    self.echo("""\
{RYou feel a hand grab you, you begin to fly upwards!
{BYou pass through the clouds and out of the world!
{GYou have rejoined Reality!

{WFor {RNews{W, {CRoleplaying {Wand {MInfo{W, Visit our website!
{Cw{cww{x.{Cw{caterdeep{x.{Co{crg{x""")

    connection = self.get_connection()
    connection.dispatch("LOGGED_OUT", {
        "name": self.name
    })
    connection.close()
