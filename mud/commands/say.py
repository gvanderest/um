def say_command(self, arguments):
    """Speak to the Room."""
    if not arguments:
        self.echo("Say what?")
        return

    message = " ".join(arguments)
    self.echo("{MYou say {x'{m%s{x'" % (message))
    self.act_to_room("{M{actor.name}{M says {x'{m{message}{x'", {
        "message": message
    })
