from utils.ansi import Ansi


def format_actor_level(actor):
    if actor.level_string:
        return actor.level_string
    # FIXME constant
    MAX_LEVEL = 101
    if actor.level == MAX_LEVEL:
        return "{BHRO"
    return "{x" + str(actor.level or 1).rjust(3)


def format_actor_gender(actor):
    if actor.gender_string:
        return actor.gender_string
    if actor.gender_id == "male":
        return "{BM"
    elif actor.gender_id == "female":
        return "{MF"
    else:
        return "{8N"


def format_actor_race(actor):
    if actor.race_string:
        return actor.race_string
    # FIXME find actual race, indexed
    from settings import RACES
    for record in RACES:
        if record["id"] == actor.race_id:
            return Ansi.pad_right(record["acronym"], 5)
    return "{CU{cndef"


def format_actor_class(actor):
    if actor.class_string:
        return actor.class_string
    # FIXME find actual race, indexed
    from settings import CLASSES
    for record in CLASSES:
        if record["id"] == actor.class_id:
            return Ansi.pad_right(record["acronym"], 3)
    return "{RA{rdv"


def format_actor_flags(actor):
    if actor.flags_string:
        return actor.flags_string

    return "{W.%s{W......" % (
        "{RP" if actor.has_flag(actor.PK_FLAG) else "{BN"
    )


def format_actor_clan(actor):
    if actor.clan_string:
        return actor.clan_string

    return Ansi.pad_right(actor.clan.acronym, 5) if actor.clan else "     "


def format_actor_who_string(actor):
    if actor.who_string:
        return actor.who_string

    return "%s {x%s {x%s {x%s" % (
        format_actor_gender(actor),
        format_actor_race(actor),
        format_actor_class(actor),
        format_actor_clan(actor),
    )


def format_afk_flag(actor):
    """Format the AFK flag for display."""
    if actor.has_flag(actor.AFK_FLAG):
        return "[{yAFK{x] "
    return ""


def who_command(self, arguments):
    """List online players."""
    # TODO check visibility
    game = self.get_game()
    connections = game.get_connections()
    count = 0
    self.echo("""\
{G                   The Visible Mortals and Immortals of Waterdeep
{g-----------------------------------------------------------------------------""")
    for connection in connections:
        if connection.state != "playing":
            continue

        actor = connection.actor

        count += 1
        self.echo("{x%s {x%s {x[%s{x] %s%s%s%s" % (
            format_actor_level(actor),
            format_actor_who_string(actor),
            format_actor_flags(actor),
            format_afk_flag(actor),
            actor.name,
            (" " + actor.title) if actor.title else "",
            (" {x[%s{x]" % (actor.bracket)) if actor.bracket else ""
        ))

    # FIXME make these numbers real
    visible_count = count
    total_count = count
    top_count = count

    self.echo()
    self.echo("{GPlayers found{g: {w%d   {GTotal online{g: {W%d   {GMost on today{g: {w%d{x" % (visible_count, total_count, top_count))
