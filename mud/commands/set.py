"""Set values on Entities in the Game."""


def set_clan_command(self, target, id):
    """Set an Actor's clan."""
    from settings import CLANS
    id = id.lower()
    if not id:
        self.echo("%s's clan set to none" % (target.name))
        target.clan_id = None
        return

    for record in CLANS:
        if record["id"] == id:
            target.clan_id = id
            self.echo("%s's clan set to: %s{x" % (target.name, record["name"]))
            return

    self.echo("Clan not found, valid options:")
    for valid_id in map(lambda x: x["id"], CLANS):
        self.echo(valid_id)


def set_race_command(self, target, id):
    """Set an Actor's race."""
    from settings import RACES
    id = id.lower()
    for record in RACES:
        if record["id"] == id:
            # FIXME set_race()
            target.race_id = id
            self.echo("%s's race set to: %s{x" % (target.name, record["name"]))
            return

    self.echo("Race not found, valid options:")
    for valid_id in map(lambda x: x["id"], RACES):
        self.echo(valid_id)


def set_class_command(self, target, class_id):
    """Set an Actor's class."""
    from settings import CLASSES
    class_id = class_id.lower()
    for record in CLASSES:
        if record["id"] == class_id:
            target.class_id = class_id
            self.echo("%s's class set to: %s{x" %
                      (target.name, record["name"]))
            return

    self.echo("Classs not found, valid options:")
    for valid_id in map(lambda x: x["id"], CLASSES):
        self.echo(valid_id)


def set_bracket_command(self, target, bracket):
    """Set an Actor's bracket."""
    target.bracket = bracket
    self.echo("%s's bracket set to: %s{x" % (target.name, bracket))


def set_who_string_command(self, target, value):
    """Set an Actor's who_string."""
    target.who_string = value
    self.echo("%s's who_string set to: %s{x" % (target.name, value))


def set_race_string_command(self, target, value):
    """Set an Actor's race_string."""
    target.race_string = value
    self.echo("%s's race_string set to: %s{x" % (target.name, value))


def set_class_string_command(self, target, value):
    """Set an Actor's class_string."""
    target.class_string = value
    self.echo("%s's class_string set to: %s{x" % (target.name, value))


def set_gender_string_command(self, target, value):
    """Set an Actor's gender_string."""
    target.gender_string = value
    self.echo("%s's gender_string set to: %s{x" % (target.name, value))


def set_flags_string_command(self, target, value):
    """Set an Actor's flags_string."""
    target.flags_string = value
    self.echo("%s's flags_string set to: %s{x" % (target.name, value))


def set_level_string_command(self, target, value):
    """Set an Actor's level_string."""
    target.level_string = value
    self.echo("%s's level_string set to: %s{x" % (target.name, value))


def set_clan_string_command(self, target, value):
    """Set an Actor's clan_string."""
    target.clan_string = value
    self.echo("%s's clan_string set to: %s{x" % (target.name, value))


def set_title_command(self, target, value):
    """Set an Actor's title."""
    target.title = value
    self.echo("%s's title set to: %s{x" % (target.name, value))


def set_command(self, arguments):
    """Set a value for an Actor field."""
    # Characters = self.game.get_injector("Characters")

    def display_help(reason=None):
        if reason:
            self.echo(reason)
            self.echo()

        self.echo("'set <name> bracket [bracket]' - Set a bracket")
        self.echo("'set <name> clan [id] - Set a clan")
        self.echo("'set <name> race <id> - Set a race")
        self.echo("'set <name> class <id> - Set a class")
        self.echo("'set <name> who_string [value] - Restring WHO basic info")
        self.echo("'set <name> gender_string [value] - Restring WHO gender")
        self.echo("'set <name> race_string [value] - Restring WHO race")
        self.echo("'set <name> class_string [value] - Restring WHO class")
        self.echo("'set <name> level_string [value] - Restring WHO level")
        self.echo("'set <name> clan_string [value] - Restring WHO clan")
        self.echo("'set <name> flags_string [value] - Restring WHO flags")

    if not arguments:
        display_help("A target must be provided")
        return

    target_name = arguments.pop(0)
    # FIXME replace with Characters injector
    target = self.game.find_player(target_name)
    if not target:
        self.echo("Player not found.")
        return

    if not arguments:
        display_help("A field must be provided.")
        return

    field = arguments.pop(0)
    value = " ".join(arguments)

    COMMANDS = (
        ("bracket", set_bracket_command),
        ("clan", set_clan_command),
        ("race", set_race_command),
        ("class", set_class_command),
        ("who_string", set_who_string_command),
        ("class_string", set_class_string_command),
        ("race_string", set_race_string_command),
        ("gender_string", set_gender_string_command),
        ("level_string", set_level_string_command),
        ("flags_string", set_flags_string_command),
        ("clan_string", set_clan_string_command),
    )
    for keyword, func in COMMANDS:
        if keyword.startswith(field):
            func(self, target, value)
            return

    display_help("The field provided does not exist.")
