"""Core Engine."""
from mud.module import Module
from mud.entities.room import rooms_injector
from mud.entities.character import Characters


class Core(Module):
    """Core functionality of the Game."""

    INJECTORS = {
        "Characters": Characters,
        "Rooms": rooms_injector,
    }
