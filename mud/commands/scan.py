def scan_command(self, arguments):
    # FIXME make this look a few rooms away
    direction = None
    self.act_to_room("{actor.name} looks all around.")

    self.echo("Looking around you see:")

    room = self.get_room()
    room_ids = []
    actors = []

    MAX_DISTANCE = 3
    stack = [(room, 0, None)]

    while stack:
        room, distance, direction_id = stack.pop()
        room_ids.append(room.id)

        for actor in room.get_actors(exclude=self):
            actors.append((actor, distance, direction_id))

        if distance >= MAX_DISTANCE:
            break

        for exit in room.get_exits():
            next_room = exit.get_room()

            if next_room.id in room_ids:
                continue

            stack.append((next_room, distance + 1, exit.direction_id))

    DISTANCES = (
        "right here",
        "nearby to the {}",
        "not far {}",
    )
    for actor, distance, direction_id in actors:
        self.echo("%s{x, %s{x." % (
            actor.name,  # TODO visibility
            DISTANCES[distance].format(direction_id)
        ))
