import math


class Map(object):
    DIRECTIONS = [
        {
            "id": "north",
            "offset": (0, -1),
            "symbols": [
                ["{x|"]
            ],
            "after": (0, -2),
        },
        {
            "id": "south",
            "offset": (0, 1),
            "symbols": [
                ["{x|"]
            ],
            "after": (0, 2),
        },
        {
            "id": "east",
            "offset": (1, 0),
            "symbols": [
                ["{x-", "-"]
            ],
            "after": (3, 0),
        },
        {
            "id": "west",
            "offset": (-2, 0),
            "symbols": [
                ["{x-", "-"]
            ],
            "after": (-3, 0),
        },
        {
            "id": "up",
            "offset": (1, -1),
            "symbols": [
                ["{Y,"]
            ],
        },
        {
            "id": "down",
            "offset": (-1, 1),
            "symbols": [
                ["{y'"]
            ],
        },
    ]

    @classmethod
    def format_room_symbol(cls, room):
        return "{C#"

    @classmethod
    def generate_map(cls, room, width=60, height=30, border=False):
        outputs = []

        for y in range(height):
            row = []
            for x in range(width):
                row.append(" ")
            outputs.append(row)

        start_x = math.floor(width / 2)
        start_y = math.floor(height / 2)

        max_x = width - 1
        max_y = height - 1

        stack = [(start_x, start_y, room)]
        first = True

        while len(stack):
            base_x, base_y, base_room = stack.pop()

            if first:
                outputs[base_y][base_x] = "{R@"
                first = False
            else:
                outputs[base_y][base_x] = cls.format_room_symbol(base_room)

            # FIXME use room.get_exits() as the basis, not the other way around
            for direction in cls.DIRECTIONS:
                exit = base_room.get_exit(direction["id"])

                if not exit:
                    continue

                offset_x, offset_y = direction["offset"]

                for symbol_y, row in enumerate(direction["symbols"]):
                    for symbol_x, symbol in enumerate(row):

                        x = base_x + symbol_x + offset_x
                        y = base_y + symbol_y + offset_y

                        if x < 0 or x > max_x or y < 0 or y > max_y:
                            continue

                        outputs[y][x] = symbol

                after = direction.get("after", None)
                if after is None:
                    continue

                after_x, after_y = after
                next_x = base_x + after_x
                next_y = base_y + after_y

                if next_x < 0 or next_x > max_x or \
                    next_y < 0 or next_y > max_y:
                    continue

                if outputs[next_y][next_x] == " ":
                    next_room = exit.get_room()
                    stack.append((next_x, next_y, next_room))

        if border:
            for y in [0, max_y]:
                for x in range(max_x + 1):
                    symbol = "-"
                    if x == 0 or x == max_x:
                        symbol = "{x+"
                    outputs[y][x] = symbol
            for y in range(1, max_y):
                for x in [0, max_x]:
                    outputs[y][x] = "{x|"

        return Map(outputs)

    def __init__(self, data):
        self.data = data

    def get_lines(self):
        return tuple(map(lambda row: "".join(row) + "{x", self.data))

    def echo_map(self, actor):
        for line in self.get_lines():
            actor.echo(line)


def map_command(self, arguments):
    """Set the command delay of this Connection."""

    room = self.get_room()
    mapped = Map.generate_map(room, width=60, height=30, border=False)
    mapped.echo_map(self)
