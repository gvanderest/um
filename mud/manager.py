"""Game Managers."""


class Manager(object):
    """
    Game Manager.

    Provide an injectable overseer that handles things on timers.
    """

    TICK_DELAY = 1.0  # measured in seconds
    HANDLES_EVENTS = []

    def __init__(self, game):
        """Instantiate the Manager."""
        self.game = game

    def get_game(self):
        """Return the Game being managed."""
        return self.game

    def start(self):
        """Initialize the Manager."""

    def stop(self):
        """Shutdown the Manager."""

    def tick(self):
        """What to do every TICK_DELAY seconds."""

    def handle_event(self, event):
        """What to do when an Event comes in."""

    def dispatch(self, *args, **kwargs):
        """Dispatch an Event to the Game."""
        self.game.dispatch(*args, **kwargs)
