def color_command(self, arguments):
    """Toggle client ANSI color."""
    self.connection.color_enabled = not self.connection.color_enabled
    if self.connection.color_enabled:
        self.echo("{BC{Ro{Yl{Co{Gr{x is now {RON{x, Way Cool!")
    else:
        self.echo("Color is now OFF, <sigh>")
