"""MUD Exceptions."""


class InvalidEvent(Exception):
    """Dispatch and various other actions require a valid Event is provided."""


class UnblockableEvent(Exception):
    """Attempting to block an Event marked as unblockable."""
