def sockets_command(self, arguments):
    game = self.get_game()

    self.echo("Sockets Listing (ID - Name - State - Hostname)")
    count = 0
    for conn in game.get_connections():
        count += 1

        name = conn.name
        state = conn.state
        hostname = conn.get_hostname()
        ip = conn.ip
        id = conn.id
        self.echo("%s - %s - %s - %s (%s)" % (id, name, state, hostname, ip))

    self.echo()
    self.echo("Number of sockets: %s" % (count))
