from mud.game_entity import GameEntity


class Area(GameEntity):
    def get_actors(self, exclude=None):
        """Generate the list of Actors/Characters in the Area."""
        # FIXME use collection to query information only
        for connection in self.game.get_connections():
            if not connection.state == "playing":
                continue
            if not connection.actor:
                continue
            actor = connection.actor
            if actor == exclude:
                continue
            room = actor.get_room()
            if self.id != room.area_id:
                continue

            yield actor
