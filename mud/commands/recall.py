def recall_command(self, arguments):
    """Recall to the start."""
    from mud.commands.look import look_command

    self.act_to_room("{actor.name} prays for transportation.")
    if not self.can_recall():
        self.echo("The gods have forsaken you.")
        return

    self.recall()
    self.act_to_room("{actor.name} appears in the room.")

    look_command(self)
