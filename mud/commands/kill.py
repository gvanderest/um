def format_damage_strength(amount, color="{B"):
    return "%sdoes {RU{rNS{wPE{WA{wKA{rBL{RE%s things to" % (
        color, color
    )

def format_incoming_damage(self, enemy, amount, noun="slash", dodged=False, parried=False, blocked=False):
    if dodged:
        return "{c%s{c dodges your attack." % (enemy.name)
    elif parried:
        return "{c%s{c parries your attack." % (enemy.name)
    elif blocked:
        return "{c%s{c blocks your attack." % (enemy.name)

    return "{c%s{c's %s{c %s{c you%s {B-{R={C%d{R={B-{x" % (
        enemy.name,
        noun,
        format_damage_strength(amount, color="{c"),
        "!" if amount > 50 else ".",
        amount
    )

def format_outgoing_damage(self, enemy, amount, noun="slash"):
    return "{BYour %s %s %s%s {B-{R={C%d{R={B-{x" % (
        noun,
        format_damage_strength(amount),
        enemy.name,
        "!" if amount > 50 else ".",
        amount
    )

def kill_command(self, arguments):
    import random

    if not arguments:
        self.echo("Kill whom?")
        return

    target = self.game.find_player(arguments[0], exclude=self)
    if not target:
        self.echo("Target not found.")
        return

    can_attack, reason = self.check_can_attack(target)
    if not can_attack:
        self.echo(reason)
        return

    # if not self.can_kill(target):
    #     self.echo("You can't attack that target.")
    #     return

    for _ in range(6):
        can_attack, _ = self.check_can_attack(target)
        if not can_attack:
            continue

        amount = random.randint(500, 600)
        self.echo(format_outgoing_damage(
            self=self,
            enemy=target,
            amount=amount,
        ))
        target.echo(format_incoming_damage(
            self=target,
            enemy=self,
            amount=amount,
        ))
        target.damage(amount)

    for _ in range(6):
        if not target.can_attack(self):
            continue

        amount = random.randint(500, 600)
        self.echo(format_incoming_damage(
            self=self,
            enemy=target,
            amount=amount,
        ))
        target.echo(format_incoming_damage(
            self=target,
            enemy=self,
            amount=amount,
        ))

        self.damage(amount)

    # KLUDGE to be fixed elsewhere
    self.set_dead(False)
    target.set_dead(False)
