def where_command(self, arguments):
    # FIXME check you can see players
    room = self.get_room()
    area = room.get_area()

    self.echo("Players near you in %s:" % (area.name))
    for actor in area.get_actors():
        actor_room = actor.get_room()
        self.echo("%-30s %s" % (actor.name, actor_room.name))
