from mud.game_entity import GameEntity
from mud.entity import Entity
from mud.game_collection import GameCollection


class Actors(GameCollection):
    INDEXES = ["room_id", "room_vnum"]


class Actor(GameEntity):
    """A creature or person existing in the Game."""
    PK_FLAG = "player_killer"
    HARDCORE_PK_FLAG = "hardcore_pk"
    ROLEPLAYER_FLAG = "roleplayer"
    ROLEPLAYING_FLAG = "roleplaying"

    def __init__(self, *args, **kwargs):
        super(Actor, self).__init__(*args, **kwargs)
        super(Entity, self).__setattr__("snoopers", [])
        self.Rooms = self.game.get_injector("Rooms")

    def can_recall(self):
        """Return boolean for being able to recall."""
        return True

    def can_attack(self, target):
        """Return boolean for being able to attack an Actor."""
        return self.check_can_attack(target)[0]

    def find_room_actor(self, arguments):
        """Find an Actor in the Room."""
        # FIXME visibility checks
        # FIXME constant
        if arguments[0] == "self":
            self.echo("RETURNING SELF")
            return self

        name = arguments[0].lower()
        room = self.get_room()

        for actor in room.get_actors():
            if actor.name.lower().startswith(name):
                return actor

        return None

    def check_can_attack(self, target):
        """Can this Actor attack a target Actor?"""
        room = self.get_room()
        if not self.has_flag(self.PK_FLAG):
            return False, "You aren't a PKiller."

        if not target.has_flag(target.PK_FLAG):
            return False, "They aren't a PKiller."

        if room.has_flag("safe"):
            return False, "You cannot attack them here."

        if self.room_id != target.room_id:
            return False, "They aren't here."

        if self.dead or target.is_dead():
            return False, "The target is already dead."

        return True, ""

    def recall(self, silent=False):
        """Send the Actor back to start."""
        # FIXME make this real
        recall_room = self.Rooms.find("abc123")
        self.set_room(recall_room)

    def damage(self, amount):
        self.current_hp -= amount
        if self.current_hp <= 0:
            self.current_hp = 1
            self.echo("You have DIED!")
            self.act_to_room("{actor.name} has been killed!")
            self.dead = True
            self.recall(silent=True)

    def set_dead(self, value):
        self.dead = bool(value)

    def is_dead(self):
        """Is this Actor dead?"""
        return bool(self.dead)

    def act_to_room(self, template, data=None, room=None, exclude=None):
        if data is None:
            data = {}
        data["actor"] = self

        if not room:
            room = self.get_room()

        excludes = [self]
        if exclude is not None:
            excludes.append(exclude)
        targets = room.get_actors(exclude=excludes)

        for target in targets:
            self.act_to(target, template, data)

    def act_to(self, target, template, data):
        message = template

        if "{actor.name}" in message:
            message = message.replace("{actor.name}", self.name)

        if "{actor.himself}" in message:
            # FIXME use gender check
            message = message.replace("{actor.himself}", "himself")

        if "{target.name}" in message:
            message = message.replace("{target.name}", data["target"].name)

        if "{target.himself}" in message:
            # FIXME gender check
            message = message.replace("{target.himself}", "himself")

        # Replace any strings
        for key, value in data.items():
            field = "{%s}" % (key)
            if field in message:
                message = message.replace(field, value)

        target.echo(message)

    def get_clan(self):
        """Get the Clan an Actor is associated with."""
        if not self.clan_id:
            return None

        # FIXME use collection
        from settings import CLANS
        for record in CLANS:
            if record["id"] == self.clan_id:
                return Clan(record, self.game)

        return None

    def remove_snooper(self, snooper):
        if snooper.connection in self.connection.snoopers:
            self.connection.snoopers.remove(snooper.connection)

    def add_snooper(self, snooper):
        if snooper.connection not in self.connection.snoopers:
            self.connection.snoopers.append(snooper.connection)

    def is_snooping(self, target):
        return self.connection in target.connection.snoopers

    def unsnoop(self, target):
        """Stop snooping a target."""
        target.remove_snooper(self)

    def snoop(self, target):
        """Start snooping a target."""
        target.add_snooper(self)

    def set_connection(self, connection):
        super(Entity, self).__setattr__('connection', connection)

    def get_connection(self):
        game = self.get_game()
        return game.get_connection(self.connection_id)

    def echo(self, *args, **kwargs):
        """Echo output to the Connection."""
        connection = self.get_connection()
        if not connection:
            return
        connection.writeln(*args, **kwargs)

    def get_room(self):
        from mud.entities.room import Room
        room = self.Rooms.find("id", self.room_id)

        if room is None:
            room = self.Rooms.find("vnum", self.room_vnum)

        if room is None:
            room = Room({
                "id": "void",
                "vnum": "void",
                "name": "The Void",
                "description": [
                    "You are hovering in nothingness.",
                    "All direction is meaningless.",
                ],
                "flags": [
                    "safe",
                ],
                "exits": {
                }
            }, self.game)

        return room

    def set_room(self, room):
        self.room_id = room.id
        self.room_vnum = room.vnum
