def room_dig_command(self, arguments):
    """Dig in a direction."""
    from settings import DIRECTIONS
    from mud.commands.direction import direction_command

    if not arguments:
        self.echo("A direction must be specified.")
        return

    found = None

    # TODO replace with a direciton finder
    for direction in DIRECTIONS:
        if direction["id"].startswith(arguments[0]):
            found = direction
            break

    if not found:
        self.echo("Direction not found.")
        return

    room = self.get_room()
    exit = room.get_exit(direction["id"])
    if exit:
        self.echo("You cannot dig in a direction with an exit.")
        return

    Rooms = self.game.get_injector("Rooms")
    new_room = Rooms.add({
        "area_vnum": room.area_vnum,
        "name": "A Freshly-Dug Room",
        "description": [
            "The walls of this room are magically carved.",
        ],
        "exits": {}
    })

    room.exits[direction["id"]] = {
        "room_id": new_room.id
    }
    new_room.exits[direction["opposite_id"]] = {
        "room_id": room.id
    }
    self.echo("New room created!")
    direction_command(self, direction)

def room_description_command(self, arguments):
    """Manipulate the description of the current room."""
    def display_help(self, reason):
        if reason:
            self.echo(reason)
            self.echo()
        self.echo("'room desc -' to remove a line.")
        self.echo("'room desc + <line>' to add a line.")
        self.echo("'room desc <line>' to replace the description.")

    if not arguments:
        display_help(self, "Description must be provided.")
        return

    action = "replace"
    if arguments[0] == "-":
        action = "remove"
        arguments.pop(0)
    elif arguments[0] == "+":
        action = "add"
        arguments.pop(0)

    line = " ".join(arguments)

    room = self.get_room()
    # FIXME use proper set_description, add_description/methods
    if action == "replace":
        room.description = [line]
    elif action == "add":
        room.description.append(line)
    elif action == "remove":
        if not room.description:
            self.echo("There are no description lines to remove.")
            return
        room.description.pop()

    self.echo("Room description updated:")
    for line in room.description:
        self.echo(line)


def room_name_command(self, arguments):
    """Set the name of the current room."""
    name = " ".join(arguments)

    if not name:
        self.echo("You must provide a name.")
        self.echo("Usage: room name <name>")
        return

    room = self.get_room()
    # FIXME use room.set_name
    room.name = name
    self.echo("Room name set to: %s{x" % (room.name))


def room_command(self, arguments):
    """Building commands."""
    def show_help(self):
        self.echo("Usage: room <command> [data]")
        self.echo("room dig <direction> - Create a new room in a direction")
        self.echo("room name <name> - Rename a room")
        self.echo("room description <commands> - Modify the room description")
        self.echo("room unlink <direction> - Not Yet Implemented")
        self.echo("room link <direction> <id> - Not Yet Implemented")

    if not arguments:
        self.echo("Please choose a room command.")
        show_help(self)
        return

    first_command = arguments.pop(0)
    if "name".startswith(first_command):
        room_name_command(self, arguments)
    elif "description".startswith(first_command):
        room_description_command(self, arguments)
    elif "dig".startswith(first_command):
        room_dig_command(self, arguments)
    else:
        self.echo("The requested command does not exist.")
        show_help(self)
