"""Settings."""
from mud.commands.commands import commands_command
from mud.commands.recall import recall_command
from mud.commands.color import color_command
from mud.commands.look import look_command
from mud.commands.flee import flee_command
from mud.commands.prompt import prompt_command
from mud.commands.kill import kill_command
from mud.commands.room import room_command
from mud.commands.quit import quit_command
from mud.commands.snoop import snoop_command
from mud.commands.set import set_command
from mud.commands.open_close import open_command
from mud.commands.open_close import close_command
from mud.commands.sockets import sockets_command
from mud.commands.title import title_command
from mud.commands.who import who_command
from mud.commands.score import score_command
from mud.commands.wizlist import wizlist_command
from mud.commands.scan import scan_command
from mud.commands.where import where_command
from mud.commands.exception import exception_command
from mud.commands.delay import delay_command
from mud.commands.equipment import equipment_command
from mud.commands.say import say_command
from mud.commands.togglepk import togglepk_command
from mud.commands.map import map_command
from mud.commands.afk import afk_command
import os

DATA_PATH = os.getcwd() + '/data'

# Telnet settings.
TELNET_BACKLOG = 1
TELNET_HOST = "0.0.0.0"
TELNET_PORT = 4200
TELNET_ENCODING = "utf8"

# Modules to install.
MODULES = (
    "modules.telnet.Telnet",
    "modules.core.Core",
)

COMMANDS = [
    {"keyword": "score", "handler": score_command},
    {"keyword": "afk", "handler": afk_command},
    {"keyword": "map", "handler": map_command},
    {"keyword": "say", "handler": say_command},
    {"keyword": "togglepk", "handler": togglepk_command},
    {"keyword": "equipment", "handler": equipment_command},
    {"keyword": "delay", "handler": delay_command},
    {"keyword": "color", "handler": color_command},
    {"keyword": "look", "handler": look_command},
    {"keyword": "flee", "handler": flee_command},
    {"keyword": "kill", "handler": kill_command, "aliases": ["murder", "battle"]},
    {"keyword": "prompt", "handler": prompt_command},
    {"keyword": "quit", "handler": quit_command},
    {"keyword": "snoop", "handler": snoop_command},
    {"keyword": "open", "handler": open_command},
    {"keyword": "set", "handler": set_command},
    {"keyword": "close", "handler": close_command},
    {"keyword": "sockets", "handler": sockets_command},
    {"keyword": "title", "handler": title_command},
    {"keyword": "who", "handler": who_command},
    {"keyword": "scan", "handler": scan_command},
    {"keyword": "where", "handler": where_command},
    {"keyword": "wizlist", "handler": wizlist_command},
    {"keyword": "exception", "handler": exception_command},
    {"keyword": "room", "handler": room_command},
    {"keyword": "commands", "handler": commands_command},
    {"keyword": "recall", "handler": recall_command},
]

CLASSES = [
    {
        "id": "warrior",
        "name": "Warrior",
        "acronym": "{RW{rar",
    },
    {
        "id": "gladiator",
        "name": "Gladiator",
        "acronym": "{BG{bla",
        "parent_id": "warrior",
    },
    {
        "id": "mage",
        "name": "Mage",
        "acronym": "{RM{rag",
    },
    {
        "id": "wizard",
        "name": "Wizard",
        "acronym": "{BW{biz",
        "parent_id": "mage",
    },
    {
        "id": "rogue",
        "name": "Rogue",
        "acronym": "{RR{rog",
    },
    {
        "id": "mercenary",
        "name": "Mercenary",
        "acronym": "{BM{ber",
        "parent_id": "rogue",
    },
    {
        "id": "druid",
        "name": "Druid",
        "acronym": "{RD{rru",
    },
    {
        "id": "sage",
        "name": "Sage",
        "acronym": "{BS{bag",
        "parent_id": "druid",
    },
    {
        "id": "cleric",
        "name": "Cleric",
        "acronym": "{RC{rle",
    },
    {
        "id": "priest",
        "name": "Priest",
        "acronym": "{BP{bri",
        "parent_id": "cleric",
    },
    {
        "id": "ranger",
        "name": "Ranger",
        "acronym": "{RR{ran",
    },
    {
        "id": "strider",
        "name": "Strider",
        "acronym": "{BS{btr",
        "parent_id": "ranger",
    },
    {
        "id": "vampire",
        "name": "Vampire",
        "acronym": "{RV{ram",
    },
    {
        "id": "lich",
        "name": "Lich",
        "acronym": "{BL{bic",
        "parent_id": "vampire",
    },
    {
        "id": "monk",
        "name": "Monk",
        "acronym": "{BM{bnk",
    },
]

RACES = [
    {
        "id": "human",
        "name": "Human",
        "acronym": "{CH{cuman"
    },
    {
        "id": "heucuva",
        "name": "Heucuva",
        "acronym": "{CH{ceucv"
    },
    {
        "id": "titan",
        "name": "Titan",
        "acronym": "{CT{citan"
    },
    {
        "id": "thrikreen",
        "name": "Thri'kreen",
        "acronym": "{CT{chken"
    },
    {
        "id": "satyr",
        "name": "Satyr",
        "acronym": "{CS{catyr"
    },
    {
        "id": "podrikev",
        "name": "Podrikev",
        "acronym": "{CP{codkv"
    },
    {
        "id": "pixie",
        "name": "Pixie",
        "acronym": "{CP{cixie"
    },
    {
        "id": "minotaur",
        "name": "Minotaur",
        "acronym": "{CM{cntur"
    },
    {
        "id": "kenku",
        "name": "Kenku",
        "acronym": "{CK{cenku"
    },
    {
        "id": "goblin",
        "name": "Goblin",
        "acronym": "{CG{cobln"
    },
    {
        "id": "gnome",
        "name": "Gnome",
        "acronym": "{CG{cnome"
    },
    {
        "id": "gnoll",
        "name": "Gnoll",
        "acronym": "{CG{cnoll"
    },
    {
        "id": "giant",
        "name": "Giant",
        "acronym": "{CG{ciant"
    },
    {
        "id": "esper",
        "name": "Esper",
        "acronym": "{CE{csper"
    },
    {
        "id": "dwarf",
        "name": "Dwarf",
        "acronym": "{CD{cwarf"
    },
    {
        "id": "drow",
        "name": "Drow",
        "acronym": "{CD{crow"
    },
    {
        "id": "tiefling",
        "name": "Tiefling",
        "acronym": "{CT{ciefl"
    },
    {
        "id": "draconian",
        "name": "Draconian",
        "acronym": "{CD{cracn"
    },
    {
        "id": "centaur",
        "name": "Centaur",
        "acronym": "{CC{centr"
    },
    {
        "id": "avian",
        "name": "Avian",
        "acronym": "{CA{cvian"
    },
    {
        "id": "halfling",
        "name": "Halfling",
        "acronym": "{CH{cflng"
    },
    {
        "id": "halforc",
        "name": "Half-Orc",
        "acronym": "{CH{c.Orc"
    },
    {
        "id": "halfelf",
        "name": "Half-Elf",
        "acronym": "{CH{c.Elf"
    },
    {
        "id": "elf",
        "name": "Elf",
        "acronym": "{CE{clf"
    },
    {
        "id": "troll",
        "name": "Troll",
        "acronym": "{CT{croll"
    },
]

CLANS = [
    {
        "id": "nightmare",
        "name": "{RC{rhurch {Ro{rf {bN{8ightmares",
        "acronym": "{RC{ro{bN{x",
    },
    {
        "id": "black_church",
        "name": "{8Black Church",
        "acronym": "{8BlkCh",
    },
    {
        "id": "radiant_heart",
        "name": "{RO{rr{yd{re{Rr{c of the {RR{rad{yi{ran{Rt H{re{ya{rr{Rt",
        "acronym": "{RR{rd{RH{rr{Rt",
    },
    {
        "id": "vector",
        "name": "{MV{mectorian {ME{mmpire",
        "acronym": "{MV{mectr",
    },
]

DIRECTIONS = [
    {
        "id": "north",
        "name": "{Rnorth",
        "opposite_id": "south"
    },
    {
        "id": "east",
        "name": "{Meast",
        "opposite_id": "west"
    },
    {
        "id": "south",
        "name": "{rsouth",
        "opposite_id": "north"
    },
    {
        "id": "west",
        "name": "{mwest",
        "opposite_id": "east"
    },
    {
        "id": "up",
        "name": "{Yup",
        "opposite_id": "down"
    },
    {
        "id": "down",
        "name": "{ydown",
        "opposite_id": "up"
    },
]

CHANNELS = [
    {
        "id": "imptalk",
        "keyword": "imptalk",
        "name": "Implementor Talk",
        "visiblity_forced": True,
        "to_self": "{y[{xIMP Talk{y] {W{actor.name}{r: {C{message}{x",
        "to_other": "{y[{xIMP Talk{y] {W{actor.name}{r: {C{message}{x",
    },
    {
        "id": "ooc",
        "keyword": "ooc",
        "name": "Out Of Character",
        "visiblity_forced": True,
        "to_self": "{WYou OOC {8'{w{message}{8'{x",
        "to_other": "{W[*OOC*]{c{actor.name} {8'{w{message}{8'{x"
    },
    {
        "id": "cgossip",
        "keyword": "cgossip",
        "name": "Clan Gossip",
        "visiblity_forced": True,
        "to_self": "You cgossip '{R{message}{x'",
        "to_other": "{actor.name}{x cgossips '{R{message}{x'"
    },
    {
        "id": "cgooc",
        "keyword": "cgooc",
        "name": "Clan Gossip (OOC)",
        "visiblity_forced": True,
        "to_self": "You cgossip [OOC] '{R{message}{x'",
        "to_other": "{actor.name}{x cgossips [OOC] '{R{message}{x'"
    },
    {
        "id": "immtalk",
        "keyword": "immtalk",
        "name": "Immortal Talk",
        "visiblity_forced": True,
        "to_self": "{x{actor.name}{x: {W{message}{x",
        "to_other": "{x{actor.name}{x: {W{message}{x",
    },
    {
        "id": "auction",
        "keyword": "auction",
        "name": "Auction",
        "visiblity_forced": True,
        "to_self": "{xYou {R<{G-{Y={MA/B{Y={G-{R> {CAuction {x'{G{message}{x'",
        "to_other": "{x{actor.name} {R<{G-{Y={MA/B{Y={G-{R> {CAuctions {x'{G{message}{x'",
    },
    {
        "id": "bid",
        "keyword": "bid",
        "name": "Bid",
        "visiblity_forced": True,
        "to_self": "{xYou {R<{G-{Y={MA/B{Y={G-{R> {CBid {x'{G{message}{x'",
        "to_other": "{x{actor.name} {R<{G-{Y={MA/B{Y={G-{R> {CBids {x'{G{message}{x'",
    },
    {
        "id": "heronet",
        "keyword": "heronet",
        "name": "Heronet",
        "visiblity_forced": True,
        "to_self": "{g[You{G Hero-Net{g]: {g'{message}{g'{x",
        "to_other": "{g[{R{actor.name}{G Hero-Nets{g]: {g'{message}{g'{x",
    },
    {
        "id": "music",
        "keyword": "music",
        "name": "Music",
        "visiblity_forced": True,
        "to_self": "You MUSIC: '{C{message}{x'",
        "to_other": "{actor.name} MUSIC: '{C{message}{x'",
    },
    {
        "id": "ask",
        "keyword": "ask",
        "name": "Ask",
        "visiblity_forced": True,
        "to_self": "You [Q/A] Ask '{Y{message}{x'",
        "to_other": "{actor.name}{x [Q/A] Asks '{Y{message}{x'",
    },
    {
        "id": "answer",
        "keyword": "answer",
        "name": "Answer",
        "visiblity_forced": True,
        "to_self": "You [Q/A] Answer '{Y{message}{x'",
        "to_other": "{actor.name}{x [Q/A] Answers '{Y{message}{x'",
    },
    {
        "id": "quote",
        "keyword": "quote",
        "name": "Quote",
        "visiblity_forced": True,
        "to_self": "You quote '{g{message}{x'",
        "to_other": "{actor.name}{x quotes '{g{message}{x'",
    },
    {
        "id": "grats",
        "keyword": "grats",
        "name": "Congratulations",
        "visiblity_forced": True,
        "to_self": "{xYou grats '{y{message}{x'",
        "to_other": "{x{actor.name} {Gg{Yr{Ra{Bt{Ms {x'{y{message}{x'",
    },
    {
        "id": "gossip",
        "keyword": "gossip",
        "name": "Gossip",
        "visiblity_forced": True,
        "to_self": "{BYou go{Wss{Bip {x'{c{message}{x'",
        "to_other": "{B{actor.name} go{Ws{Cs{Wi{Bps {x'{c{message}{x'",
    },
    {
        "id": "qgossip",
        "keyword": "qgossip",
        "name": "Quest Gossip",
        "visiblity_forced": True,
        "to_self": "You {C({Wqg{Bo{bs{Bs{Wip{C) {x'{C{message}{x'",
        "to_other": "{x{actor.name} {C({Wqg{Bo{bss{Bi{Wps{C) {x'{C{message}{x'",
    },
    {
        "id": "clan",
        "keyword": "clan",
        "name": "Clan Chat",
        "visiblity_forced": True,
        "send_check": lambda actor: actor.clan_id is not None,
        "send_error": "You aren't in a clan.",
        "receive_check": lambda actor, target: actor.clan_id == target.clan_id,
        "to_self": "You clan {x'{M{message}{x'",
        "to_other": "{x{actor.name} clans {x'{M{message}{x'",
    },
]

EQUIPMENT_SLOTS = [
    {"id": "neck"},
    {"id": "ear1", "label": "l. ear"},
    {"id": "ear2", "label": "r. ear"},
    {"id": "face"},
    {"id": "lip"},
    {"id": "neck1", "label": "neck"},
    {"id": "neck2", "label": "neck"},
    {"id": "clan"},
    {"id": "religion", "label": "rel. symbol"},
    {"id": "tattoo"},
    {"id": "torso"},
    {"id": "body"},
    {"id": "back"},
    {"id": "arms"},
    {"id": "wrist1", "label": "l. wrist"},
    {"id": "wrist2", "label": "r. wrist"},
    {"id": "hands"},
    {"id": "finger1", "label": "l. finger"},
    {"id": "finger2", "label": "r. finger"},
    {"id": "waist"},
    {"id": "legs"},
    {"id": "ankle1", "label": "l. ankle"},
    {"id": "ankle2", "label": "r. ankle"},
    {"id": "feet"},
    {"id": "surrounding"},
    {"id": "light"},
    {"id": "floating"},
    {"id": "weapon1", "label": "pri. weapon"},
    {"id": "shield"},
    {"id": "held"},
    {"id": "weapon2", "label": "sec. weapon"},
    {"id": "id"},
    {"id": "airship_key"},
]

SOCIALS = [
    {
        "keyword": "nod",
        # SELF
        "self_self": "You nod to yourself.",
        "actor_actor": "{actor.name} nods to {actor.himself}.",
        # ROOM
        "self_room": "You nod emphatically to everyone.",
        "actor_room": "{actor.name} nods emphatically.",
        # TARGET
        "self_target": "You nod at {target.name}.",
        "actor_target": "{actor.name} nods at {target.name}.",
        "actor_self": "{actor.name} nods at you.",
    }
]
