"""Game Modules."""


class Module(object):
    """
    Game Module.

    Provide extensions to the Game.
    """

    ID = "unknown"
    NAME = "Unknown"
    VERSION = "0.0.0"
    AUTHORS = []
    REQUIRES = {}

    MANAGERS = []
    INJECTORS = {}
    COMMANDS = []
