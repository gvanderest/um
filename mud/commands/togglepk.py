def togglepk_command(self, arguments):
    """Toggle PK mode."""
    if self.has_flag(self.PK_FLAG):
        self.remove_flag(self.PK_FLAG)
        self.echo("PK Removed")
    else:
        self.add_flag(self.PK_FLAG)
        self.echo("PK Activated")
