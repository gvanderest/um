# CHANGELOG

The changes, as they occur, between versions.

## 0.1.1

## 0.1.0 - 2016-09-22T07:03:22

- Initial versions of telnet server being worked worked upon
- Game and global event system logic started
- Module system design underway
