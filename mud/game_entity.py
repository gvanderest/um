from mud.entity import Entity


class GameEntity(Entity):
    def __init__(self, data, game):
        super(GameEntity, self).__init__(data)
        self.set_game(game)

    def set_game(self, game):
        """Set the Game this Entity belongs to."""
        super(Entity, self).__setattr__('game', game)

    def get_game(self):
        """Get the Game this Entity belongs to."""
        return self.game

    def get_flags(self):
        """Get all flags on this Entity."""
        return self.get("flags", [])

    def toggle_flag(self, flag):
        """Toggle a specific flag on this Entity."""
        if self.has_flag(flag):
            self.remove_flag(flag)
        else:
            self.add_flag(flag)

    def remove_flag(self, flag):
        """Remove a specific flag from this Entity."""
        flags = self.get_flags()
        if flag in flags:
            flags.remove(flag)
        self.flags = flags

    def add_flag(self, flag):
        """Add a specific flag to this Entity."""
        flags = self.get_flags()
        if flag not in flags:
            flags.append(flag)
        self.flags = flags

    def has_flag(self, flag):
        """Does this Entity have a flag?"""
        return flag in self.get_flags()
