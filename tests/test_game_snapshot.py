"""Test Game Snapshotting."""
from mud.game import Game


class TestGameSnapshot(object):
    """Test Game's ability to snapshot state and revert to it."""

    def setUp(self):
        """Instantiate Game."""
        self.game = Game()

    def test_snapshot_and_reversion(self):
        """Test create and revert of snapshots."""
        self.game.set_state({
            "abc": 123
        })

        state = self.game.get_state()
        assert state["abc"] == 123

        snapshot = self.game.create_snapshot()
        assert isinstance(snapshot, str)

        self.game.clear_state()

        state = self.game.get_state()
        assert len(state) == 0

        self.game.load_snapshot(snapshot)

        state = self.game.get_state()
        assert state["abc"] == 123
