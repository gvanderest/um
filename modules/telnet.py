"""Telnet server."""
from gevent import monkey
from mud.connection import Connection
from mud.commands.channel import channel_command
from mud.commands.direction import direction_command
from mud.commands.social import social_command
from mud.game_entity import GameEntity
from mud.manager import Manager
from mud.module import Module
from settings import TELNET_HOST, TELNET_BACKLOG, TELNET_PORT, TELNET_ENCODING
from utils.ansi import Ansi
from mud.entity import Entity
from settings import CHANNELS, COMMANDS, DIRECTIONS, SOCIALS
from utils.hash import get_random_hash

import gevent
import socket
import time

monkey.patch_all()


class Clan(GameEntity):
    pass

class Connection(Connection):
    """A Telnet Connection."""

    DEFAULT_INPUT_DELAY = 0.5
    MIN_INPUT_DELAY = 0.3
    OUTPUT_FLUSH_DELAY = 0.01
    MAX_INPUT_BUFFER_LENGTH = 10240
    MAX_INPUT_QUEUE_LENGTH = 40

    def __init__(self, server, conn, addr):
        """Initialize the Connection, passing up the address."""
        self.id = get_random_hash()
        self.color_enabled = True
        self.player_inputted_since_last_output = False
        self.server = server

        self.socket = conn
        self.set_ip(addr[0])
        self.port = addr[1]

        self.actor = None

        self.input_handler_thread = None
        self.output_flush_thread = None
        self.last_input = ""
        self.snoopers = []
        self.command_delay_override = None

        self.name = "Unknown"

        self.next_echo_hidden = False
        self.next_echo_hidden_reason = ""
        self.dispatch = server.dispatch

        self.input_buffer = ""
        self.output_buffer = []

        self.state = None

    def set_ip(self, ip):
        self.ip = ip
        try:
            self.host = socket.gethostbyaddr(self.ip)[0]
        except Exception:
            self.host = self.ip

    def set_command_delay(self, value):
        """Set the input delay override."""
        self.command_delay_override = value

    def get_hostname(self):
        """Get the hostname for the Connection."""
        return self.host

    def get_ip(self):
        """Get the IP address for the Connectin."""
        return self.host

    def read(self):
        """Read from the socket."""
        try:
            data = self.socket.recv(1024)
        except BlockingIOError as e:
            return ""
        except OSError:
            return None

        if not data:
            return None

        try:
            message = data.decode(TELNET_ENCODING)
        except UnicodeDecodeError:
            message = ""

        return message

    def close(self):
        """Destroy the socket."""
        self.state = "closing"
        try:
            self.flush_output()
        except Exception:
            pass

        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except Exception:
            pass

        try:
            self.socket.close()
        except Exception:
            pass

    def write_prompt(self):
        """Write the user's prompt to socket."""
        actor = self.get_actor()
        if actor.has_flag(actor.AFK_FLAG):
            self.write("<AFK> ")
            return

        message = actor.prompt

        # If the prompt doesn't end with a newline, add a space
        if not message.endswith("%c"):
            message += " "

        room = actor.get_room()

        exit_string = ""
        for direction in DIRECTIONS:
            exit = room.get_exit(direction["id"])
            if not exit:
                continue
            exit_string += direction["id"][0].upper()

        replaces = {
            "%g": "{G1234{x",
            "%h": actor.current_hp,
            "%H": actor.hp,
            "%m": actor.current_mana,
            "%M": actor.mana,
            "%N": self.name,
            "%c": "\n",
            "%v": actor.current_moves,
            "%V": actor.moves,
            "%x": "1234",
            "%X": "0",
            "%a": "0",
            # TODO check visibility
            "%r": room.name,
            "%e": "NESWUD",
            "%R": room.get("vnum", "unknown"),
            "%z": room.get("area_vnum", "unknown"),
            "%j": "10",
            "%J": "10",
            "%q": "10",
            "%Q": "10",
            "%t": "12am",
        }

        for key, value in replaces.items():
            if key not in message:
                continue
            message = message.replace(key, str(value))

        self.write("\n")
        self.write(message)

    def write_prompt_if_needed(self):
        """Write the user's prompt if they're in a valid situation to do so."""
        actor = self.get_actor()
        if self.state != "playing" or not actor:
            return

        if not actor.prompt_enabled:
            return

        self.write_prompt()

    def get_actor(self):
        return self.actor

    def display_motd(self):
        """Display the Message of the Day."""
        self.writeln("Message of the Day:")
        self.writeln("Recent Changes:")
        self.writeln("{G* {xYou can toggle PK and 'kill' each other")
        self.writeln("{G* {xAdded minimap/map command")
        self.writeln("{Y* {xPerformance increases")

    def go_to_motd(self):
        self.state = "motd"
        self.display_motd()
        self.writeln()
        self.write("[Press Enter to Continue] ")

    def handle_input(self, message):
        """Handle an input line, which was terminated with a newline."""
        game = self.get_game()
        Characters = game.get_injector("Characters")
        import re
        self.player_inputted_since_last_output = True
        cleaned = message.strip().lower()

        if self.state == "username":
            # FIXME allow platforms to bridge our game and provide IP/hostname
            if message.startswith("@"):
                if message.startswith("@ip"):
                    parts = message.split(" ")
                    self.set_ip(parts[1])
                return

            name = cleaned.title()
            if not re.search('^[A-Z][a-z]{2,9}$', name):
                self.writeln("Your name must be 3-10 characters long, made only of letters.")
                self.write("What is your name? ")
                self.dispatch("FAILED_USERNAME")
                return

            existing = Characters.find(name)
            if existing:
                self.writeln("Sorry, that name is already in use.")
                self.write("Please pick another: ")
                return

            self.create_character(name)
            self.go_to_motd()
            return

            self.writeln("This is a new name to our world.")
            self.write("Are you sure your name is %s? [Y/n] " % (self.name))
            self.state = "create_confirm_username"

        elif self.state == "create_confirm_username":
            if cleaned and cleaned[0] != "y":
                self.write("What's your name? ")
                self.state = "username"
                return

            self.write("Choose a password: ")
            self.state = "create_password"
            self.hide_next_input("Creating Password")

        elif self.state == "create_password":
            self.writeln()
            self.password = cleaned
            self.write("Confirm Password: ")
            self.state = "create_confirm_password"
            self.hide_next_input("Confirm New Password")

        elif self.state == "create_confirm_password":
            self.writeln()

            if cleaned != self.password:
                self.writeln("Your password and confirmation must match.")
                self.write("Create Password: ")
                self.state = "create_password"
                self.hide_next_input("Create Password")
                return

            actor = self.create_character(self.name)

            self.write("\n\nTest MOTD.\n\n[Press Enter to Continue] ")
            self.state = "motd"

        elif self.state == "motd":
            self.state = "playing"
            self.handle_input("look")
            self.dispatch("LOGGED_IN", {
                "name": self.name
            })

        elif self.state == "playing":
            # FIXME clean this up?
            self.write_to_snoopers("%s Input> %s" % (self.name, message))

            if not message:
                self.writeln()
                return

            if message == "!":
                return self.handle_input(self.last_input)

            self.last_input = message

            parts = message.strip().split()
            command = parts.pop(0)
            if not command:
                return
            arguments = parts
            try:
                self.handle_and_time_command(command, arguments)
            except Exception:
                self.server.game.handle_exception()

    def create_character(self, name):
        actor_data = {
            "id": name,
            "room_vnum": "3014",
            "room_id": "abc123",
            "name": name,
            "race_id": "human",
            "class_id": "cleric",
            "prompt": "{8[{R%h{8/{r%H{8h {B%m{8/{b%M{8m {M%v{8v "
                      "{W%N{8({Y%X{8) {W%r{8({w%q{8/{w%t{8) {W%a{8]{x",
            "prompt_enabled": True,
            "hp": 4000,
            "current_hp": 4000,
            "mana": 4000,
            "current_mana": 4000,
            "moves": 4000,
            "current_moves": 4000,
            "hitroll": 300,
            "damroll": 300,
            "spellhit": 300,
            "spelldam": 300,
            "armor": 300,
            "resist": 300,
            "avoidance": 300,
        }

        self.name = name
        game = self.get_game()
        Characters = game.get_injector("Characters")
        actor = Characters.add(actor_data)

        self.actor = actor
        self.actor.connection_id = self.id

        return actor

    def get_game(self):
        return self.server.get_game()

    def handle_and_time_command(self, command, arguments):
        start = time.time()
        self.handle_command(command, arguments)
        self.writeln("Duration: %0.4fs" % (time.time() - start))

    def handle_command(self, command, arguments):
        """Handle a command and its arguments."""
        self.write("\n")

        actor = self.get_actor()

        # TODO replace with a direciton finder
        for direction in DIRECTIONS:
            if direction["id"].startswith(command):
                direction_command(actor, direction)
                return

        for entry in COMMANDS:
            keywords = [entry["keyword"]] + entry.get("aliases", [])
            for keyword in keywords:
                if keyword.startswith(command):
                    entry["handler"](actor, arguments)
                    return

        for channel in CHANNELS:
            if channel["keyword"].startswith(command):
                message = " ".join(arguments)
                channel_command(actor, channel, message)
                return

        for social in SOCIALS:
            if social["keyword"].startswith(command):
                target = None
                if arguments:
                    target = actor.find_room_actor(arguments)
                    if target is None:
                        actor.echo("They're not here.")
                        return

                social_command(actor, social, target)
                return

        actor.echo("Huh?")

    def write_connect_banner(self):
        """Display the banner at login."""
        game = self.server.get_game()
        version = game.get_version()
        self.write("""\

            ;::::;
           ;::::; :;
         ;:::::'   :;
        ;:::::;     ;.
       ,:::::'       ;           OOO\\
       ::::::;       ;          OOOOO\\
       ;:::::;       ;         OOOOOOOO     Powered by Undermountain v%s
      ,;::::::;     ;'         / OOOOOOO    Owned and Operated by Kelemvor
    ;:::::::::`. ,,,;.        /  / DOOOOOO    <immortals@waterdeep.info>
  .';:::::::::::::::::;,     /  /    DOOOO
 ,::::::;::::::;;;;::::;,   /  /       DOOO
;`::::::`'::::::;;;::::: ,#/  /        DOOO
:`:::::::`;::::::;;::: ;::#  /          DOOO
::`:::::::`;:::::::: ;::::# /            DOO
`:`:::::::`;:::::: ;::::::#/             DOO
 :::`:::::::`;; ;:::::::::##              OO
 ::::`:::::::`;::::::::;:::#              OO
 `:::::`::::::::::::;'`:;::#              O
  `:::::`::::::::;' /  / `:#
   ::::::`:::::;'  /  /   `#
     ::::: ## :: ##  ####  ###### ###### #####  ######  ###### ###### #####
       ::: ## '  ## ##  ##   ##   ##     ##  ##  ##  ## ##     ##     ##  ##
        :  ## ## ## ######   ##   ####   ##  ##  ##  ## ####   ####   #####
           ## ## ## ##  ##   ##   ##     #####   ##  ## ##     ##     ##
            ##  ##  ##  ##   ##   ###### ##  ## ######  ###### ###### ##

                          C I T Y   O F   S P L E N D O R S

                                   [ Est 1997 ]

""" % (version))
        self.write("What is your name, traveler? ")

    def handle_next_input(self):
        """Cycle through the input buffer on a thread until done."""
        INPUT_NEWLINE = "\n"
        if INPUT_NEWLINE in self.input_buffer:
            index_of_newline = self.input_buffer.index(INPUT_NEWLINE)
            line = self.input_buffer[0:index_of_newline]
            self.input_buffer = self.input_buffer[index_of_newline + 1:]
            self.dispatch("HANDLING_INPUT", {
                "line:": line
            })

            delay = self.handle_input(line)

            self.flush_output()

            if delay is None:
                delay = self.DEFAULT_INPUT_DELAY

            delay = max(delay, self.MIN_INPUT_DELAY)

            if self.command_delay_override is not None:
                delay = self.command_delay_override

            if delay == 0:
                self.handle_next_input()
            else:
                gevent.sleep(delay)

    def flush_output_interval(self):
        """On a loop, flush output."""
        while self.running:
            self.flush_output()
            # FIXME constant
            gevent.sleep(0.01)

    def handle_input_interval(self):
        """On a loop, handle next input buffer entry."""
        while self.running:
            self.handle_next_input()
            # FIXME constant for minimum check delay
            gevent.sleep(0.01)

    def start(self):
        """Start a new Connection."""
        self.state = "username"
        self.running = True
        self.write_connect_banner()
        gevent.spawn(self.flush_output_interval)
        gevent.spawn(self.handle_input_interval)

        while self.running:
            message = self.read()
            if message is None:
                self.running = False
                break
            elif message == "":
                gevent.sleep(0.05)
                continue

            hidden = "*** HIDDEN: {} ***".format(self.next_echo_hidden_reason)
            self.dispatch("TELNET_INPUT_RECEIVED", {
                "state": self.state,
                "message": hidden if self.next_echo_hidden else message
            })

            # Remove \r because nobody cares about it
            message = message.replace("\r\n", "\n")

            self.input_buffer += message

            if self.input_buffer_is_over_spam_tolerance():
                self.writeln("You have been disconnected for spamming.")
                break

            # Enable echo again.
            if self.next_echo_hidden:
                self.show_next_input()

            gevent.sleep(0.05)

        self.close()

    def input_buffer_is_over_spam_tolerance(self):
        """Return boolean for input buffer tolerances."""
        # Too much content
        if len(self.input_buffer) > self.MAX_INPUT_BUFFER_LENGTH:
            return True

        # Too many commands
        if self.input_buffer.count("\n") > self.MAX_INPUT_QUEUE_LENGTH:
            return True

        # All good, for now
        return False

    def hide_next_input(self, reason="Hidden"):
        """Hide the next input by the Player."""
        self.next_echo_hidden = True
        self.next_echo_hidden_reason = reason
        self.socket.send(b"\xFF\xFB\x01")

    def show_next_input(self):
        """Show the next input by the Player."""
        self.next_echo_hidden = False
        self.next_echo_hidden_reason = ""
        self.socket.send(b"\xFF\xFC\x01")

    def flush_output(self):
        """Flush output buffer to socket and clear thread holder."""
        if not self.output_buffer:
            return

        # TODO properly detect if output is a result of user commands, if not
        # then it should be prepended with a newline.. for wiznets/etc.
        self.write_prompt_if_needed()

        message = ""
        if not self.player_inputted_since_last_output:
            message += "\n\n"
        self.player_inputted_since_last_output = False

        message += "".join(self.output_buffer)
        self.output_buffer = []

        try:
            self.socket.send(message.encode(TELNET_ENCODING))
        except Exception as e:
            self.close()

        self.write_to_snoopers(message)

    def write(self, message=""):
        """Write a message to the socket."""
        if self.color_enabled:
            message = Ansi.colorize(message)
        else:
            message = Ansi.decolorize(message)

        # Clean up the newlines and returns
        message = message.replace("\r", "").replace("\n", "\r\n")

        self.output_buffer += message
        self.mark_connection_dirty()

    def mark_connection_dirty(self):
        """Mark this Connection as dirty and requiring a flush."""
        self.server.mark_connection_dirty(self)

    def write_to_snoopers(self, message):
        actor = self.get_actor()
        if self.snoopers:
            # TODO use constant for newlines
            snoop_prefix = "%s> " % (actor.name)
            snoop_message = message.replace("\n", "\n" + snoop_prefix)

            for snooper in self.snoopers:
                snooper.write(message=snoop_message)

    def writeln(self, message="", **kwargs):
        """Write a message to the socket."""
        self.write(message=message + "\n", **kwargs)

    def handle_event(self, event):
        """Handle an Event that has occurred."""
        if self.state != "playing":
            return

        if event.type == "EXCEPTION":
            cleaned = event.data["traceback"].replace("{", "{{")
            self.writeln("{Y--> {xEXCEPTION: %s" % (cleaned))

        elif event.type == "FAILED_USERNAME":
            self.writeln("{Y--> {xAn invalid username was provided.")

        elif event.type == "TELNET_CONNECTED":
            self.writeln("{Y--> {xA telnet connection has been created.")

        elif event.type == "TELNET_DISCONNECTED":
            self.writeln("{Y--> {xA telnet connection has been destroyed.")

        elif event.type == "LOGGED_IN":
            data = event.data
            name = data["name"]
            if name != self.name:
                self.writeln("{Y--> {x%s has logged in." % (name))

        elif event.type == "LOGGED_OUT":
            data = event.data
            name = data["name"]
            if name != self.name:
                self.writeln("{Y--> {x%s rejoins the real world." % (name))


class Server(Manager):
    """Server for accepting Telnet connections."""

    TICK_DELAY = 0.1
    DELAY_BETWEEN_CONNECTIONS = 0.1
    HANDLES_EVENTS = [
        "EXCEPTION",
        "TELNET_DISCONNECTED",
        "TELNET_CONNECTED",
        "LOGGED_IN",
        "LOGGED_OUT",
    ]

    def start(self):
        """Listen for socket connections."""
        self.running = True
        self.connections = []
        self.dirty_connections = []
        gevent.spawn(self.listen)

    def add_connection(self, connection):
        """Add the Connection."""
        self.connections.append(connection)
        self.game.add_connection(connection)

    def remove_connection(self, connection):
        """Remove a Connection."""
        self.game.remove_connection(connection)
        self.connections.remove(connection)

    def listen(self):
        """Listen for connections."""
        config = {
            "host": TELNET_HOST,
            "port": TELNET_PORT,
            "backlog": TELNET_BACKLOG,
        }

        self.dispatch("TELNET_SERVER_STARTING", config)

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        s.setblocking(0)

        s.bind((config["host"], config["port"]))
        s.listen(config["backlog"])

        self.dispatch("TELNET_SERVER_STARTED", config)
        while self.running:
            try:
                conn, addr = s.accept()
                conn.setblocking(0)
                injectors = {
                    "conn": conn,
                    "addr": addr,
                }
                gevent.spawn(lambda: self.inject(self.handle_socket, injectors))
            except BlockingIOError:
                # Waiting for Connection.
                pass
            gevent.sleep(self.DELAY_BETWEEN_CONNECTIONS)

    def inject(self, func, *args, **kwargs):
        game = self.get_game()
        game.inject(func, *args, **kwargs)

    def handle_event(self, event):
        """Handle an incoming Event from the Game."""
        for connection in self.connections:
            connection.handle_event(event)

    def handle_socket(self, conn, addr, Characters):
        """Handle a new socket connection."""
        connection = Connection(self, conn, addr)
        self.add_connection(connection)
        connection_data = {
            "ip": connection.ip,
            "host": connection.host,
            "port": connection.port,
        }
        self.dispatch("TELNET_CONNECTED", connection_data)
        connection.start()
        self.remove_connection(connection)
        actor = connection.get_actor()
        if actor:
            Characters.remove(actor)
            self.dispatch("TELNET_DISCONNECTED", connection_data)

    def mark_connection_dirty(self, connection):
        """Add Connection to the list of dirty (un-flushed) ones."""
        if connection not in self.dirty_connections:
            self.dirty_connections.append(connection)

    def stop(self):
        """Stop the Manager."""
        self.running = False

    def tick(self):
        """Flush connections."""
        self.flush_dirty_connections()

    def flush_dirty_connections(self):
        """Cycle through Connections and flush data."""
        for connection in self.dirty_connections:
            if not isinstance(connection, Connection):
                continue

            connection.flush()

        self.dirty_connections = []


class Telnet(Module):
    """Module for configuring Telnet server and connections."""

    MANAGERS = [
        Server
    ]
