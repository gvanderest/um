# Undermountain Python MUD Engine

## Initial Setup

Requires Python 3.5.2+

### Create Virtual Environment

This will allow our MUD's code and packages to run in a sandbox. By running this code separate from all other Python installations and other packages, we will ensure versions will not conflict.

```
$ virtualenv -p python3 venv
```

After this environment has been created, you will always need to activate the virtual environment before running anything, and the following line will look very repetative:

```
$ source venv/bin/activate
```

### Install Required External Packages

```
$ source venv/bin/activate
$ pip install -r requirements.txt
```

## Creating Environment and Starting Game

```
$ source venv/bin/activate
$ ./um create example
$ ./um start example
```

## Testing

You can run the unit tests via nose2 using a shell script that aliases some of the common configuration settings.

```
$ source venv/bin/activate
$ ./test
```

To verify things worked as expected, browse to the following path in your browser of choice:

```
/path/to/code/htmlcov/index.html
```

## Class Design

Borrowing design elements from [redux](https://github.com/reactjs/redux), we are creating a predictable state container for the World's data. Events are dispatched through the World and are cycled through the reducers.

- Module

  - Events - Things that globally happen in the World with provided data.
  - Injectors - Services that aspects of the World can use.
  - Commands - Handlers for Actor input.
  - Managers - Background services that tick and fire Events.

```python
from mud.module import Module
from mud.manager import Manager

class SomethingManager(Manager):
    """A Manager ticks on a delay and runs the tick() command, injected."""
    TICK_DELAY = 1.0  # Defaults to 1.0 seconds.

    def tick(self, Characters):
        self.dispatch("SOME_KIND_OF_EVENT", {
            "some_key": 123
        })

    def handle_event(self, event, Characters):
        """Handle Events as they come in, injected."""
        if event.type == "SOME_KIND_OF_EVENT":
            # ... We could do some logic to the World now..
            ch = Characters.get("Kelemvor")
            for item in ch.query_inventory():
                new_value = item.get_stat("durability", 0) + 1
                item.set_stat("durability", new_value)

class Example(Module):
    EVENTS = [
        "SOME_KIND_OF_EVENT",
    ]

    INJECTORS = {
        "Characters": Example.Characters,
    }

    COMMANDS = {
        "look": Example.look_command,
    }

    MANAGERS = [
        Example.SomethingManager,
    ]
```
