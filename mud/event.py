"""MUD Events."""
from mud.exceptions import UnblockableEvent


class Event(object):
    """Things that can occur in the Game to be handled by Managers/etc."""

    def __init__(self, type, data=None, blockable=False):
        """Create the Game and its expected services."""
        self.type = type
        self.data = data or {}
        self.blockable = blockable
        self.blocked = False

    def block(self):
        """Block Event from future firings."""
        if not self.blockable:
            raise UnblockableEvent("Event of type '{}' is unblockable.".format(
                self.type
            ))

        self.blocked = True

    def is_blocked(self):
        """Return state of blocking."""
        return self.blocked
