"""Test Events."""
from mud.exceptions import UnblockableEvent
from mud.event import Event


class TestEvents(object):
    """Test Injectors from Game work properly."""

    def test_blocking(self):
        """Test attempting to block an unblockable Event."""
        event = Event("example", blockable=True)
        event.block()

        assert event.is_blocked()

    def test_unblockable(self):
        """Test attempting to block an unblockable Event."""
        event = Event("example", blockable=False)
        failed = False
        try:
            event.block()
        except UnblockableEvent:
            failed = True

        assert failed
        assert not event.is_blocked()
