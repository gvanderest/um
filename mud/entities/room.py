"""Room."""

from utils.listify import listify
from mud.game_collection import GameCollection
from mud.game_entity import GameEntity
from mud.entities.area import Area


class RoomExit(GameEntity):
    def __init__(self, *args, **kwargs):
        super(RoomExit, self).__init__(*args, **kwargs)
        self.Rooms = self.game.get_injector("Rooms")

    def has_flag(self, flag):
        return flag in self.get("flags", [])

    def get_room(self):
        return self.Rooms.find(self.room_id)

    def remove_flag(self, flag):
        if flag in self.flags:
            self.flags.remove(flag)

    def add_flag(self, flag):
        if flag not in self.flags:
            self.flags.append(flag)


class Room(GameEntity):
    def get_area(self):
        return Area({
            "id": self.area_id,
            "vnum": self.area_vnum,
            "name": self.area_vnum.title(),
        }, self.game)

    def has_flag(self, flag):
        return flag in self.get("flags", [])

    def get_exits(self):
        """Get a list of all exits for the Room."""
        return tuple(map(self.get_exit, self.exits))

    def get_exit(self, direction):
        # FIXME likely can short-circuit this
        exits = self.get("exits", {})
        exit = exits.get(direction, None)

        if exit is None:
            return None

        # FIXME do this earlier? maybe in Room init?
        if "direction_id" not in exit:
            exit["direction_id"] = direction

        return RoomExit(exit, self.game)

    def get_actors(self, exclude=None):
        """Generate the list of Actors/Characters in the Room."""
        # FIXME use injectors?
        game = self.get_game()
        Characters = game.get_injector("Characters")
        excludes = listify(exclude)
        excluded_ids = [actor.id for actor in excludes]
        print(excluded_ids)

        for actor in Characters.query("room_id", self.id):
            print("found", actor)
            if actor.id in excluded_ids:
                print("EXCLUDED")
                continue
            yield actor


class Rooms(GameCollection):
    WRAPPER_CLASS = Room


def rooms_injector(game):
    collection = Rooms(game)
    collection.add({
        "id": "xyz321",
        "vnum": "3013",
        "area_id": "westbridgexyz",
        "area_vnum": "westbridge",
        "name": "Not Market Square",
        "flags": [],
        "description": [
            "You're no longer at Market Square.",
        ],
        "exits": {
            "south": {
                "room_id": "abc123",
                "room_vnum": "3014",
                "flags": []
            },
        }
    })
    collection.add({
        "id": "abc123",
        "vnum": "3014",
        "name": "Market Square",
        "area_id": "westbridgexyz",
        "area_vnum": "westbridge",
        "flags": ["safe"],
        "description": [
            "You're at Market Square.",
        ],
        "exits": {
            "north": {
                "room_id": "xyz321",
                "room_vnum": "3013",
                "flags": []
            },
            "down": {
                "room_id": "arena123",
                "room_vnum": "430",
                "flags": ["door", "closed"],
            }
        }
    })
    collection.add({
        "id": "arena123",
        "vnum": "430",
        "area_id": "wbextra123",
        "area_vnum": "wbextra",
        "name": "Coliseum of Pain",
        "flags": [],
        "description": [
"You stand in a large underground lobby. This is the lobby of the arena where",
"all pkillers come to battle against each other. The walls are carved out of",
"the stone that was below the Market Square. You see a Souvenir Shop to the",
"west and the Manager's Office to the east. The Arena is to the south.",
        ],
        "exits": {
            "up": {
                "room_id": "abc123",
                "room_vnum": "3014",
                "flags": ["door", "closed", "secret"],
            },
        }
    })
    return collection
