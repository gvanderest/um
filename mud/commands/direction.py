def direction_command(self, direction):
    from mud.commands.look import look_command

    direction_id = direction["id"]
    current_room = self.get_room()
    exit = current_room.get_exit(direction_id)

    if exit is None:
        self.echo("Alas, you can't go that way.")
        return

    if exit.has_flag("closed"):
        self.echo("The door is closed.")
        return

    target_room = exit.get_room()
    if target_room is None:
        raise Exception("Target room not found.")

    self.act_to_room("{actor.name}{x leaves %s{x." % (direction["name"]), {
        "direction_name": direction["name"]
    })

    self.set_room(target_room)

    self.act_to_room("{actor.name}{x has arrived.", {
        "direction_name": direction["name"]
    })

    look_command(self)
