def social_command(self, social, target):
    """Perform a social action."""

    if target is None:
        self.echo(social["self_room"])
        self.act_to_room(social["actor_room"])
        return

    if target == self:
        self.echo(social["self_self"])
        self.act_to_room(social["actor_actor"])
        return

    act_data = {
        "target": target
    }
    self.act_to(self, social["self_target"], act_data)
    self.act_to(target, social["actor_self"], act_data)
    self.act_to_room(social["actor_target"], act_data, exclude=target)
