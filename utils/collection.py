"""
Collection.

An indexing container for dictionaries to facilitate easy lookup by multiple
types of indexes.

TODO: Split indexes out, so that they can be modularized
TODO: Add support for list() of basic index values
TODO: Add support for list() of string keywords
"""
from glob import glob

import gevent
import os
import random
import json


class InvalidIndexKey(Exception):
    """The key being used is not a valid index."""


class RecordNotFound(Exception):
    """The record being searched for was not found in the Collection."""


class UniqueIndexCollision(Exception):
    """Two records are attempting to index at the same unique key-value."""


class InvalidValue(Exception):
    """Value being provided is invalid."""


class Collection(object):
    """Collection."""

    NAME = None
    PRIMARY_KEY = "id"
    INDEXES = []
    STRING_INDEXES = []
    UNIQUE_INDEXES = ["id"]

    WRAPPER_CLASS = None

    def __init__(self, storage=None):
        """Instantiate the Collection for a Game and its State data."""
        self.set_storage(storage)
        self.create_index_containers()

    def create_index_containers(self):
        """Generate the basic containers for all indexing."""
        self.indexes = {}
        for key in self.get_all_index_keys():
            self.indexes[key] = {}

    def set_storage(self, storage=None):
        """Set the storage dictionary and create collection node."""
        # TODO test for dictionary?
        storage = storage if storage is not None else {}
        self.storage = storage

    def get_storage(self):
        """Get the internal storage."""
        return self.storage

    def get_records(self):
        """Return all records as a list."""
        return self.storage

    def is_empty(self):
        """Return the emptiness state of the Collection."""
        return len(self.storage) == 0

    @classmethod
    def get_all_index_keys(cls):
        """Return all of the keys that can be used for indexing as a list."""
        # Always includes the primary key.
        return (
            cls.UNIQUE_INDEXES +
            cls.INDEXES +
            cls.STRING_INDEXES
        )

    def index_all(self, record):
        """Cycle through indexes and apply them as needed."""
        for key in self.get_all_index_keys():
            self.index(record, key)

    def unique_index(self, record, key):
        """Index a record by unique key."""
        record_id = record[self.PRIMARY_KEY]
        value = record[key]
        index = self.indexes[key]

        # Test for collisions.
        if index.get(value, None) is not None:
            message = "Index '{}' value '{}' can only be indexed once.".format(
                key, value)
            raise UniqueIndexCollision(message)

        index[value] = record_id

    def basic_index(self, record, key):
        """Index a record by basic key."""
        value = record[key]
        record_id = record[self.PRIMARY_KEY]
        index = self.indexes[key]
        index[value] = index.get(value, [])
        index[value].append(record_id)

    def string_index(self, record, key):
        """Index a record by a string key."""
        index = self.indexes[key]
        record_id = record[self.PRIMARY_KEY]

        words = set()
        value = record[key]

        # Convert a list into a flat string.
        if isinstance(value, list):
            value = ' '.join(value)

        if isinstance(value, str):
            for word in value.split(' '):
                words.add(word)
        else:
            raise InvalidValue("String index value must be list or string.")

        if words:
            partials = set()  # Unique list of partial words.
            for word in words:
                partial = ""
                for letter in word:
                    partial += letter
                    partials.add(partial)

            for partial in partials:
                partial_index = index.get(partial, None)
                if partial_index is None:
                    partial_index = []
                    index[partial] = partial_index
                partial_index.append(record_id)

    def index(self, record, key):
        """Index a record by a key."""
        # One.
        if key in self.UNIQUE_INDEXES:
            return self.unique_index(record, key)

        # Multiple.
        if key in self.INDEXES:
            return self.basic_index(record, key)

        # String indexes.
        if key in self.STRING_INDEXES:
            return self.string_index(record, key)

    def deindex_all(self, record):
        """Cycle through indexes and remove them as needed."""
        for key in self.get_all_index_keys():
            self.deindex(record, key)

    def deindex(self, record, key=None):
        """Deindex a record by a key."""
        # FIXME rather than extract primary key each time, just provide it?
        record_id = record[self.PRIMARY_KEY]
        index = self.indexes[key]
        value = record[key]

        # One.
        if key in self.UNIQUE_INDEXES:
            del index[value]

        # Multiple.
        elif key in self.INDEXES:
            index[value] = index.get(value, [])
            index[value].remove(record_id)

        # String indexes.
        elif key in self.STRING_INDEXES:
            # FIXME handle lists or string with split words?
            # FIXME handle invalid types
            words = record[key]
            words = words.split() if isinstance(words, str) else words
            partials = set()
            for word in words:
                for end_position in range(1, len(word) + 1):
                    partial = word[0:end_position]
                    partials.add(partial)

            for partial in partials:
                partial_index = index[partial]
                partial_index.remove(record_id)

    @classmethod
    def get_random_hash(cls):
        """Get a randomized hash."""
        return "%040x" % random.getrandbits(40 * 4)

    def apply_hash_if_needed(self, record):
        """Add a hash if the record is lacking one."""
        if record.get(self.PRIMARY_KEY) is not None:
            return

        unique_value = self.get_random_hash()
        record[self.PRIMARY_KEY] = unique_value

    def add(self, record):
        """Add a record to the Collection, returning the updated dictionary."""
        # TODO check for dictionary?

        if not isinstance(record, dict):
            record = record.to_dict()

        self.apply_hash_if_needed(record)
        record_id = record.get(self.PRIMARY_KEY)
        self.storage[record_id] = record
        self.index_all(record)
        return self.find(record_id)

    @classmethod
    def check_key_is_valid_index(cls, key):
        """Raise exception if key is an invalid index."""
        if key not in cls.get_all_index_keys():
            message = "The key '{}' is not indexed.".format(key)
            raise InvalidIndexKey(message)

    def find(self, key, value=None):
        """Find a single record."""
        # If only one argument is provided, assume PRIMARY_KEY.
        if value is None:
            value = key
            key = self.PRIMARY_KEY

        for result in self.query(key, value):
            return result

        return None

    def wrap_record_in_class(self, record):
        """Return a wrapped record, if there is an entity class."""
        return self.WRAPPER_CLASS(record)

    def query(self, key=None, value=None, predicate=None):
        """Generate a list of search results."""
        self.check_key_is_valid_index(key)

        if not value:
            return []

        record_ids = []

        # Only one.
        if key in self.UNIQUE_INDEXES:
            record_id = self.indexes[key].get(value, None)
            if record_id is not None:
                record_ids = [record_id]

        # A list!
        elif key in self.INDEXES:
            record_ids = self.indexes[key].get(value, [])

        # String compares!
        elif key in self.STRING_INDEXES:
            # Find the shortest list
            words = value.lower().split()
            if len(words):
                # FIXME store length instead of looking it up each time?
                # FIXME find the shortest list?
                lists = self.indexes[key]
                while len(words):
                    word = words.pop(0)
                    record_ids = lists.get(word, [])
                    if len(record_ids) == 0:
                        break

        # Finally, iterate through record IDs to yield results.
        return [
            self.wrap_record_in_class(self.storage[_id]) for _id
            in record_ids
        ]

    def remove(self, record):
        """Remove a record from the Collection."""
        if not isinstance(record, str):
            record = self.find(record[self.PRIMARY_KEY])
        else:
            record = self.find(record)

        # FIXME handle failed search with exception?
        if record is None:
            raise RecordNotFound("The record being removed does not exist in "
                                 "this Collection.")

        self.deindex_all(record)
        del self.storage[record[self.PRIMARY_KEY]]


class FileCollection(Collection):
    """Persistent Collection that saves to files."""

    FILE_SUFFIX = ".json"
    SAVE_INTERVAL = 1.0

    def __init__(self, path=".", cache=True, cache_timeout=1.0, **kwargs):
        """Initialize folder path and cache partial data."""
        super(FileCollection, self).__init__(**kwargs)

        self.cache = cache
        self.cache_timeout = cache_timeout

        self.set_path(path)

        self.running = True
        gevent.spawn(self.save_interval)

    def save_interval(self):
        """Async save interval to update files."""
        while self.running:
            gevent.sleep(self.SAVE_INTERVAL)
            self.save_records()

    def save_records(self):
        """For each record, save it to file."""
        symbol = os.sep

        for _, record in self.storage.items():
            filename = self.get_filename(record)
            path = self.path + symbol + filename
            contents = json.dumps(record, indent=4)
            with open(path, "w") as fh:
                fh.write(contents)

    @classmethod
    def get_filename(cls, record):
        """Function used for getting the filename of the record."""
        return record[cls.PRIMARY_KEY] + cls.FILE_SUFFIX

    def set_path(self, path):
        """Set the data path and create folders needed."""
        symbol = os.sep
        path = path.rstrip(symbol)

        if not os.path.exists(path):
            os.makedirs(path)

        self.path = path
        self.load_data()

    def load_data(self):
        """Using the data folder provided, load the files."""
        for path in glob(self.path + "/*" + self.FILE_SUFFIX):
            with open(path, "r") as fh:
                contents = fh.read()
                record = json.loads(contents)
                self.add(record)
