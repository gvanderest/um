from mud.entities.actor import Actor, Actors
from mud.game_collection import GameCollection


class Character(Actor):
    pass

class Characters(Actors):
    WRAPPER_CLASS = Character
