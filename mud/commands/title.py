def title_command(self, arguments):
    title = ' '.join(arguments)
    self.title = title
    self.echo("Title set to: %s{x" % (title))
