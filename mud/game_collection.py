from utils.collection import Collection


class GameCollection(Collection):
    def __init__(self, game):
        super(GameCollection, self).__init__()
        self.game = game

    def wrap_record_in_class(self, record):
        return self.WRAPPER_CLASS(record, self.game)
