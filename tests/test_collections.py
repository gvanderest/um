"""Test Collections."""
from utils.collection import Collection, InvalidIndexKey, RecordNotFound, \
    UniqueIndexCollision, InvalidValue, FileCollection

import gevent
import json
import os
import time
import shutil


class DerpClass(object):
    """A basic Entity."""

    def __init__(self, data):
        """Create an internal data reference."""
        self.data = data if data is not None else {}
        self.get = self.data.get

    def __getattr__(self, *args, **kwargs):
        """Return internal data value."""
        return self.data.get(*args, **kwargs)

    def __getitem__(self, *args, **kwargs):
        """Return internal data value."""
        return self.__getattr__(*args, **kwargs)


class DerpClassTwo(DerpClass):
    """Employ a to_dict and from_dict."""

    @classmethod
    def from_dict(cls, data):
        """Convert from a dictionary."""
        return cls(data)

    def to_dict(self):
        """Convert to a dictionary."""
        return self.data


class CustomStorageCollection(Collection):
    """A Collection that uses provided storage."""

    NAME = "custom"


class BasicTestCollection(Collection):
    """A basic Collection, no extra fluff."""

    pass


class DifferentPrimaryKeyCollection(Collection):
    """A Collection with a different primary key."""

    PRIMARY_KEY = "banana"
    UNIQUE_INDEXES = ["banana"]


class StringTestCollection(Collection):
    """A Collection that tests keyword indexing."""

    STRING_INDEXES = ["keywords"]


class UniqueTestCollection(Collection):
    """A Collection that tests unique indexing."""

    UNIQUE_INDEXES = ["owner_id"]


class WrapperCollection(Collection):
    """A Collection that wraps results."""

    WRAPPER_CLASS = DerpClass


class DeeperWrapperCollection(Collection):
    """A Collection that wraps results with to/from_dict."""

    WRAPPER_CLASS = DerpClassTwo


class TestCollections(object):
    """Test indexing and add/removal of data from Game."""

    def setUp(self):
        """Instantiate a basic Game and some example data."""
        self.dragonkind_example = {
            "name": "The Shimmering Sword of the Dragonkind",
            "keywords": "sword dragonkind kind dragon shimmering",
            "owner_id": "abc123",
            "description": "This sword is really cool and looks like dragons.",
            "value": 10000
        }

    def test_hash_generator(self):
        """Test that hashes are always conforming to rules."""
        valid_letters = '0123456789abcdef'
        for _ in range(10000):
            random_hash = Collection.get_random_hash()
            for letter in random_hash:
                assert letter in valid_letters
            assert len(random_hash) == 40

    def test_collection_basics(self):
        """Add, index, and remove data."""
        collection = BasicTestCollection()

        example = dict(self.dragonkind_example)

        assert example.get("id") is None

        collection.add(example)

        # Increment count by one.
        assert len(collection.get_records()) == 1
        assert not collection.is_empty()

        # Should have created a SHA1 hash
        example_id = example.get("id")
        assert isinstance(example_id, str)
        assert len(example_id) == 40

        # Should not be found.
        found = collection.find("id", "bananarama")
        assert found is None

        # Search for it by the ID that just got generated.
        found = collection.find("id", example_id)
        assert found is example

        # Remove it by ID, all records should be gone now.
        collection.remove(example)
        assert len(collection.get_records()) == 0
        assert collection.is_empty()

        # Add it back in, which should leave the ID unchanged.
        collection.add(example)
        assert example.get("id") == example_id
        collection.remove(example)

    def test_different_primary_key(self):
        """Test that fetching by a unique index works properly."""
        collection = DifferentPrimaryKeyCollection()
        example = {}
        collection.add(example)

        assert example.get("id") is None
        assert example.get("banana") is not None

    def test_unique_index_collision(self):
        """Test that double-indexing fails."""
        collection = UniqueTestCollection()
        example = dict(self.dragonkind_example)
        collection.add(example)

        failed = False
        try:
            collection.add(example)
        except UniqueIndexCollision:
            failed = True

        assert failed

    def test_unique_index_find(self):
        """Test that fetching by a unique index works properly."""
        collection = UniqueTestCollection()
        example = dict(self.dragonkind_example)
        collection.add(example)

        # Now search for the object by owner_id
        found = collection.find("owner_id", "abc123")
        assert found is example

    def test_string_index_find(self):
        """String keyword indexing for find lookups."""
        collection = StringTestCollection()
        example = dict(self.dragonkind_example)
        collection.add(example)

        # Now search by single keywords.
        found = collection.find("keywords", "sword")
        assert found is example
        found = collection.find("keywords", "dragonkind")
        assert found is example
        found = collection.find("keywords", "kind")
        assert found is example
        found = collection.find("keywords", "dragon")
        assert found is example
        found = collection.find("keywords", "shimmering")
        assert found is example

        # Partial keywords.
        found = collection.find("keywords", "swo")
        assert found is example
        found = collection.find("keywords", "dragonk")
        assert found is example
        found = collection.find("keywords", "ki")
        assert found is example
        found = collection.find("keywords", "dra")
        assert found is example
        found = collection.find("keywords", "shi")
        assert found is example

        # Endings of keywords should fail.
        found = collection.find("keywords", "word")
        assert found is None
        found = collection.find("keywords", "ragonkind")
        assert found is None
        found = collection.find("keywords", "ind")
        assert found is None
        found = collection.find("keywords", "ragon")
        assert found is None
        found = collection.find("keywords", "himmering")
        assert found is None

        # All keywords.
        found = collection.find(
            "keywords",
            "sword dragonkind kind dragon shimmering")
        assert found is example

        # All keywords in reverse.
        found = collection.find(
            "keywords",
            "shimmering dragon kind dragonkind sword"
        )
        assert found is example

    def test_multiple_string_indexed_objects(self):
        """Test weird combinations of string indexes."""
        collection = StringTestCollection()
        first_dragonkind = dict(self.dragonkind_example)
        collection.add(first_dragonkind)
        second_dragonkind = dict(self.dragonkind_example)
        collection.add(second_dragonkind)
        fake_sword = {
            "keywords": "fake sword"
        }
        collection.add(fake_sword)
        swear_jar = {
            "keywords": "swear jar"

        }
        collection.add(swear_jar)

        swords = list(collection.query("keywords", "sword"))
        assert len(swords) == 3
        sword_ids = [record["id"] for record in swords]
        assert first_dragonkind["id"] in sword_ids
        assert second_dragonkind["id"] in sword_ids
        assert fake_sword["id"] in sword_ids

        dragonkinds = list(collection.query("keywords", "dragonkind"))
        assert len(dragonkinds) == 2
        dragonkind_ids = [record["id"] for record in dragonkinds]
        assert first_dragonkind["id"] in dragonkind_ids
        assert second_dragonkind["id"] in dragonkind_ids

        sws = list(collection.query("keywords", "sw"))
        assert len(sws) == 4

    def test_provided_storage(self):
        """Test that a Collection can be used on a provided storage."""
        storage = {}
        collection = CustomStorageCollection(storage=storage)

        assert storage is collection.get_storage()

        example = dict(self.dragonkind_example)

        records = collection.get_records()
        assert len(records) == 0
        assert collection.is_empty()

        collection.add(example)

        records = collection.get_records()
        assert len(records) == 1
        assert not collection.is_empty()

        collection.remove(example)

        records = collection.get_records()
        assert len(records) == 0
        assert collection.is_empty()

    def test_array_of_strings(self):
        """Test a list of strings being indexed."""
        collection = StringTestCollection()

        example = {
            "name": "Some Goblin Sword",
            "keywords": ["some", "goblin", "sword"]
        }
        collection.add(example)

        sword = collection.find("keywords", "goblin")
        assert sword is not None
        assert sword["name"] == "Some Goblin Sword"

    def test_string_index_invalid_value(self):
        """Test a bad value for string indexing."""
        collection = StringTestCollection()
        bad_data = {
            "keywords": 1234
        }
        failed = False

        try:
            collection.add(bad_data)
        except InvalidValue:
            failed = True

        assert failed

    def test_big_string_index(self):
        """Test a big list of items."""
        collection = StringTestCollection()
        quantity = 10000
        examples = []
        for _ in range(quantity):
            example = {
                "keywords": "sword of the dragonkind"
            }
            collection.add(example)
            examples.append(example)

        assert not collection.is_empty()

        results = collection.query("keywords", "sword dragonkind")
        assert len(results) == quantity

        results = collection.query("keywords", "sword of the dragonkind")
        assert len(results) == quantity

        for _ in range(quantity):
            example = examples.pop(0)
            collection.remove(example)

        assert collection.is_empty()

    def test_wrapping_of_records(self):
        """Test that returned records are wrapped."""
        collection = WrapperCollection()

        # Add the example data.
        example = dict(self.dragonkind_example)
        example_id = collection.add(example)

        # We should now have a record.
        records = collection.get_records()
        assert not collection.is_empty()
        assert len(records) == 1

        # We should have gotten back an object.
        entity = collection.find(example_id)
        assert isinstance(entity, DerpClass)
        assert entity.id == example_id

        # Remove it using the object/entity.
        collection.remove(entity.id)

        # Now it should be empty.
        records = collection.get_records()
        assert collection.is_empty()
        assert len(records) == 0

        # And not be able to be found.
        record = collection.find(example_id)
        assert record is None

    def test_wrapping_of_with_magic(self):
        """Test that returned records are wrapped to to/from-dict functions."""
        collection = DeeperWrapperCollection()

        example = self.dragonkind_example
        wrapped = DerpClassTwo(example)
        record_id = collection.add(wrapped)
        found = collection.find("id", record_id)

        assert found.id == record_id
        assert found["id"] == record_id
        assert found.to_dict() == example

        collection.remove(wrapped)
        assert collection.is_empty()

    def test_invalid_key_indexing(self):
        """Test an invalid key being referenced."""
        class InvalidKeyCollection(Collection):
            pass

        collection = InvalidKeyCollection()
        failed = False
        try:
            collection.add({"abc": 123})
            collection.query("fakeIndex", 123)
        except InvalidIndexKey:
            failed = True

        assert failed

    def test_querying_with_no_value(self):
        """Test no value provided in query."""
        collection = Collection()
        collection.add({})
        empty = collection.query("id")
        assert len(empty) == 0

    def test_basic_indexing(self):
        """Test a basic index scenario."""
        class BasicIndexCollection(Collection):
            INDEXES = ["room_id"]

        collection = BasicIndexCollection()
        ruby = {
            "name": "ruby",
            "room_id": 123
        }
        collection.add(ruby)
        emerald = {
            "name": "emerald",
            "room_id": 123
        }
        collection.add(emerald)

        gems = collection.query("room_id", 123)
        assert len(gems) == 2
        assert gems[0]["name"] == "ruby"
        assert gems[1]["name"] == "emerald"

        collection.remove(ruby)
        collection.remove(emerald)

    def test_removal_of_non_existent_record_by_id(self):
        """Test the attempted removal non-existent record by id."""
        collection = Collection()
        failed = False
        try:
            collection.remove("abc123")
        except RecordNotFound:
            failed = True

        assert failed

    def test_removal_of_non_existent_record(self):
        """Test the attempted removal of record that isn't collected."""
        fake_record = {
            "id": "not_real"
        }
        collection = Collection()
        failed = False
        try:
            collection.remove(fake_record)
        except RecordNotFound:
            failed = True

        assert failed


class KeywordsFileCollection(FileCollection):
    """A FileCollection that indexes keywords."""

    SAVE_INTERVAL = 1.0
    STRING_INDEXES = ["keywords"]


class TestFileCollections(object):
    """Test the FileCollection works properly."""

    def setUp(self):
        """Set up the data directory, and some stuff."""
        self.current_dir = os.path.dirname(__file__)
        self.data_dir = self.current_dir + "/data/examples"
        assert not os.path.exists(self.data_dir)
        self.collection = KeywordsFileCollection(path=self.data_dir)

        self.dragonkind = {
            "id": "dragonkind",
            "keywords": "sword dragonkind",
            "name": "The Sword of the Dragonkind",
        }

    def tearDown(self):
        """Unlink a folder."""
        shutil.rmtree(self.current_dir + "/data")

    def test_folder_created(self):
        """Test the path exists."""
        assert os.path.exists(self.data_dir)

    def test_files_loaded_on_creation(self):
        """Test that files in folder are loaded on creation."""
        assert self.collection.is_empty()

        file_path = self.data_dir + "/dragonkind.json"

        with open(file_path, "w") as fh:
            fh.write(json.dumps(self.dragonkind))

        self.collection = KeywordsFileCollection(path=self.data_dir)

        first_key = list(self.collection.storage)[0]
        first_record = self.collection.storage[first_key]
        assert first_record.get("id") is not None
        assert first_record.get("keywords") == "sword dragonkind"
        assert first_record.get("name") == "The Sword of the Dragonkind"

        assert not self.collection.is_empty()

        found = self.collection.find("keywords", "sword dragonkind")
        assert found.get("id") == first_record.get("id")
        assert found.get("keywords") == "sword dragonkind"
        assert found.get("name") == "The Sword of the Dragonkind"

        assert os.path.exists(file_path)

        self.collection.remove(found)

    def test_save_interval(self):
        """Test that the saves occur on interval properly."""
        file_path = self.data_dir + "/dragonkind.json"

        with open(file_path, "w") as fh:
            fh.write(json.dumps(self.dragonkind))

        mtime = os.path.getmtime(file_path)

        self.collection = KeywordsFileCollection(path=self.data_dir)

        gevent.sleep(0.5)

        # Not modified yet.
        new_mtime = os.path.getmtime(file_path)
        assert mtime == new_mtime

        gevent.sleep(0.5)

        # Now it should be.
        final_mtime = os.path.getmtime(file_path)
        assert mtime != final_mtime

        # Add a new record.
        record_id = self.collection.add({
            "name": "Cloak of Kelemtor",
            "keywords": "cloak kelemvor"
        })

        gevent.sleep(1.0)

        # Should be a new file now.
        new_file_path = self.data_dir + "/" + record_id + ".json"
        assert os.path.exists(new_file_path)

    # def test_partial_loading(self):
    #     """
    #     Test that partial loading works as expected.
    #
    #     Test that a file loaded into the Collectio only loads partially until
    #     the data is requested, then it is fully loaded.
    #     """
    #     # Should start empty.
    #     assert self.collection.is_empty()
    #
    #     # Pre-load data into datafile.
    #     file_path = self.data_dir + "/dragonkind.json"
    #     with open(file_path, "w") as fh:
    #         fh.write(json.dumps(self.dragonkind))
    #
    #     # Load Collection with caching enabled.
    #     self.collection = KeywordsFileCollection(
    #         path=self.data_dir,
    #         cache=True,
    #         cache_timeout=1.0,
    #     )
    #
    #     first_key = list(self.collection.storage)[0]
    #
    #     # Partial view should be available directly from storage.
    #     first_record = self.collection.storage[first_key]
    #     assert first_record.get("id") is not None
    #     assert first_record.get("keywords") == "sword dragonkind"
    #     assert first_record.get("name") is None
    #
    #     # Full view should be available via search.
    #     found = self.collection.find("keywords", "sword dragonkind")
    #     assert found.get("id") == first_record.get("id")
    #     assert found.get("keywords") == "sword dragonkind"
    #     assert found.get("name") == "The Sword of the Dragonkind"
    #
    #     # Sleep for half the time of cache_timeout.
    #     gevent.sleep(0.5)
    #
    #     # Should still be a full view, as this expires after 1.0 seconds
    #     assert first_record.get("id") is not None
    #     assert first_record.get("keywords") == "sword dragonkind"
    #     assert first_record.get("name") is None
    #
    #     # Passed expiration threshold, should now be expired.
    #     gevent.sleep(0.51)
    #
    #     # Should be back to the basic view
    #     first_record = self.collection.storage[first_key]
    #     assert first_record.get("id") is not None
    #     assert first_record.get("keywords") == "sword dragonkind"
    #     assert first_record.get("name") is None
