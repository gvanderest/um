def prompt_command(self, arguments):
    if not arguments:
        self.prompt_enabled = not self.prompt_enabled
        if self.prompt_enabled:
            self.echo("Prompt is now shown.")
        else:
            self.echo("Prompt is now hidden.")
        return

    prompt = ' '.join(arguments)
    self.prompt = prompt
    self.echo("Prompt set to: %s{x" % (prompt))
